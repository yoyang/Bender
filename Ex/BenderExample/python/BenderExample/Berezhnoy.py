#!/usr/bin/env python
# ============================================================================
## @file
#  helper test-file to  detect "bad" vs "good"  versions of Bender/DaVinci/Analysis/Phys/...
# 
#  For "bad" versions the B2OC stripping efficiencies for
#  tro channels 
#  \f$ \Lambda_b^0 \rightarrow ( \Lambda_c^+ \rightarrow p K^- \pi^+ ) \pi^+ \pi^- \pi^-\f$ and
#  \f$ \Lambda_b^0 \rightarrow ( D^+ \rightarrow K^- \pi^+ \pi^+ ) p \pi^- \pi^- \f$ 
#  ...
#  are very different, while for "good" versions they are very close..
# =============================================================================
import ROOT, math 
from   Bender.MainMC import *
from Bender.Logger import getLogger 
if '__main__' == __name__ : logger = getLogger ( 'BenderExample.Berezhnoy' )
else                      : logger = getLogger ( __name__ )
# =============================================================================

AlgoMC._recinfo_init_ = False

class MyAlgMC1(AlgoMC) :
    
    # Main function
    def analyse(self) :


            cands_mcsel = self.mcselect ( 'candidates_mcsel', '[Lambda_b0 ==> (Lambda_c+ ==>  p+  K-  pi+) pi- pi+ pi-]CC')
            if 0 == cands_mcsel.size() : return SUCCESS

            tup_mcsel = self.nTuple('lb2lcpipipi_mcsel')
            for Lb in cands_mcsel:
                
                tup_mcsel.column_int   ( 'cands_size_mcsel', cands_mcsel.size()     )
                tup_mcsel.column_float ( 'pt_Lb_mcsel'     , MCPT ( Lb ) / GeV      )
                tup_mcsel.column_float ( 'y_Lb_mcsel'      , MCY  ( Lb )            )
                tup_mcsel.write ()

            return  SUCCESS

class MyAlgMC2(AlgoMC) :
    
        # Main function
    def analyse(self) :


            cands_mcsel = self.mcselect ( 'candidates_mcselect', '[Lambda_b0 ==>   (D+ ==> K- pi+ pi+)  p+  pi-  pi-]CC' )
            if 0 == cands_mcsel.size() : return SUCCESS

            tup_mcsel = self.nTuple('lb2dppipi_mcsel')
            for Lb in cands_mcsel: 
                tup_mcsel.column_int   ( 'cands_size_mcsel', cands_mcsel.size()     )
                tup_mcsel.column_float ( 'pt_Lb_mcsel'     , MCPT ( Lb ) / GeV      )
                tup_mcsel.column_float ( 'y_Lb_mcsel'      , MCY  ( Lb )            )
                tup_mcsel.write ()

            return SUCCESS 

class MyAlg1(AlgoMC) :
    
    def all_masses ( self, particle, products, others ) :
        #
        # Returns all patricle subgroups for the process
        # something ->  (particle -> products) + others.
        #
        all_particles=[particle]+products+others
        masses,up,down=[all_particles],[all_particles],[]

        for x1 in all_particles:
            for x2 in up:
                for x3 in x2:
                    tmp=x2[:]
                    tmp.remove(x3)
                    if tmp not in down: down.append(tmp)
            masses += down
            up=down
            down=[]

        n_products=len(products)
        for x in masses[:]:  
            n_intersections=len(set(x).intersection(products))
            if any( [
                    n_products==n_intersections, 
                    (particle in x) and n_intersections>0,
                    len(x)<2 
                    ] ):
                masses.remove(x)

        return masses   

    def invmassn(self, particles) :
        p4 = Gaudi.LorentzVector()
        for p in particles :
            p4 += p.momentum()
        return p4.M()
         

    def initialize ( self ) :
        
        sc = AlgoMC.initialize ( self )
        if sc.isFailure() : return sc

                   
        triggers = {}
        triggers['Lb']= {}
        triggers['Lc' ]= {}
            
        lines    = {}
        lines [ "Lb"  ] = {}
        lines [ "Lb" ][   'L0TOS' ] = 'L0HadronDecision'
        lines [ "Lb" ][   'L0TIS' ] = 'L0(Hadron|DiMuon|Muon|Electron|Photon)Decision'
            
        lines [ "Lb" ][ 'Hlt1TOS' ] = 'Hlt1TrackAllL0Decision'
        lines [ "Lb" ][ 'Hlt1TIS' ] = 'Hlt1(DiMuon|SingleMuon|Track).*Decision'
            
        lines [ "Lb" ][ 'Hlt2TOS' ] = 'Hlt2(Topo2|Topo3|Topo4|Charm2)BodyBBDTDecision'
        lines [ "Lb" ][ 'Hlt2TIS' ] = 'Hlt2(Charm|Topo|DiMuon|Single).*Decision'
            
        sc = self.tisTos_initialize ( triggers , lines )
        if sc.isFailure () : return sc
        

        self.dtf_chi2_fun      = DTF_CHI2NDOF ( self , True , strings('Lambda_c+'))
        self.dtf_chi2_nod_fun  = DTF_CHI2NDOF ( self , True )

        self.dtf_m_fun         = DTF_FUN  ( self , M , True, strings('Lambda_c+'))
        self.dtf_m_nod_fun     = DTF_FUN  ( self , M , True)
        
        self.dtf_ctau_fun      = DTF_CTAU ( self , ABSID == 'Lambda_b0', True, strings('Lambda_c+'))
        self.dtf_ctau_nod_fun  = DTF_CTAU ( self , ABSID == 'Lambda_b0', True)

        self.dtf_dctau_fun     = DTF_CTAU ( self , ABSID == 'Lambda_c+', True, strings('Lambda_c+'))
        self.dtf_dctau_nod_fun = DTF_CTAU ( self , ABSID == 'Lambda_c+', True)
        
        self.ipchi2_fun  = BPVIPCHI2 ( self  )
        self.bpvdira     = BPVDIRA   ( self  )
        self.allmasses = self.all_masses('Lc',['Lcp','LcK','Lcpi'],['pim1','pi','pim2'])
       
        return SUCCESS


    
    # Main function
    def analyse(self) :
        
        cands = self.select ( 'candidates' , '[ Beauty -> ( Lambda_c+ -> p+ K- pi+ )  ( a_1(1260)- -> pi- pi+ pi- )]CC')
        cands_size=cands.size()
        
        if 0 == cands_size : return SUCCESS
        
        rc_summary   = self.get( '/Event/Rec/Summary' ).summaryData()
        odin         = self.get( '/Event/DAQ/ODIN'    )
        
        tup = self.nTuple('lb2lcpipipi')
        
        mcLb  = self.mcselect  ( "mcLb"  , '[Lambda_b0 ==>  (Lambda_c+ ==>  p+ K- pi+)  pi-  pi+  pi-]CC' )        
        mcLc  = self.mcselect  ( "mcLc"  , '[Lambda_b0 ==> ^(Lambda_c+ ==>  p+ K- pi+)  pi-  pi+  pi-]CC' )            
        mcLcp = self.mcselect  ( "mcLcp" , '[Lambda_b0 ==>  (Lambda_c+ ==> ^p+ K- pi+)  pi-  pi+  pi-]CC' )
        mcpi  = self.mcselect  ( "mcpi"  , '[Lambda_b0 ==>  (Lambda_c+ ==>  p+ K- pi+) ^pi- ^pi+ ^pi-]CC' )
        
        tLb   = MCTRUTH ( mcLb  , self.mcTruth() ) if mcLb   else PNONE
        tLc   = MCTRUTH ( mcLc  , self.mcTruth() ) if mcLc   else PNONE
        tLcp  = MCTRUTH ( mcLcp , self.mcTruth() ) if mcLcp  else PNONE
        tpi   = MCTRUTH ( mcpi  , self.mcTruth() ) if mcpi   else PNONE
        
        for Lb in cands :
            
            Lc = Lb.child(1)
            a1 = Lb.child(2)
            
            Lcp   = Lc.child(1)
            LcK   = Lc.child(2)
            Lcpi  = Lc.child(3)
            
            pim1  = a1.child(1)
            pi    = a1.child(2)
            pim2  = a1.child(3)
            
            ## add some reco-summary information 
            self.addRecSummary ( tup , rc_summary   )
            
            ## ODIN
            tup.column_aux    ( odin )
            
            tup.column_float ( 'ipchi2_Lb'   , self.ipchi2_fun ( Lb   )  ) 
            tup.column_float ( 'ipchi2_pim1' , self.ipchi2_fun ( pim1 )  ) 
            tup.column_float ( 'ipchi2_pi'   , self.ipchi2_fun ( pi   )  ) 
            tup.column_float ( 'ipchi2_pim2' , self.ipchi2_fun ( pim2 )  )
            tup.column_float ( 'ipchi2_Lcp'  , self.ipchi2_fun ( Lcp  )  ) 
            tup.column_float ( 'ipchi2_LcK'  , self.ipchi2_fun ( LcK  )  ) 
            tup.column_float ( 'ipchi2_Lcpi' , self.ipchi2_fun ( Lcpi )  )
            
            tup.column_float ( 'dtf_m'       , self.dtf_m_fun(Lb)        )
            tup.column_float ( 'dtf_m_nod'   , self.dtf_m_nod_fun(Lb)    )
            
            tup.column_float ( 'dtf_chi2'    , self.dtf_chi2_fun(Lb)     )
            tup.column_float ( 'dtf_chi2_nod', self.dtf_chi2_nod_fun(Lb) )
            
            tup.column_float ( 'ctau'        , self.dtf_ctau_fun(Lb))
            tup.column_float ( 'ctau_nod'    , self.dtf_ctau_nod_fun(Lb) )
            
            tup.column_float ( 'd_ctau'      , self.dtf_dctau_fun(Lb)    )
            tup.column_float ( 'd_ctau_nod'  , self.dtf_dctau_nod_fun(Lb))
            
            tup.column_float ( 'dira'        , self.bpvdira ( Lb )       )        
            tup.column_float ( 'd_dira'      , slef.bpvdira ( Lc )       )
            
            tup.column_int   ( 'cands_size'  , int(cands_size)           )
            
            
            true_Lb = tLb ( Lb )
            true_Lc = tLb ( Lc )
            true_PI = tpi ( pim1 ) and tpi ( pim2 ) and tpi ( pi ) 
            
            tup.column_bool ( 'mcLb'    , tLb  ( Lb   ) )
            tup.column_bool ( 'mcLc'    , tLc  ( Lc   ) )
            tup.column_bool ( 'mcLcp'   , tLcp ( Lcp  ) )
            tup.column_bool ( 'mcpim1'  , tpi  ( pim1 ) )
            tup.column_bool ( 'mcpim2'  , tpi  ( pim2 ) )
            tup.column_bool ( 'mcpi'    , tpi  ( pi   ) )
            
            if true_Lb :
                self.decisions ( Lb , self.triggers['Lb'] )
                self.decisions ( Lc , self.triggers['Lc' ] )
                                
            if true_Lb and true_Lc and true_PI :
                self.plot( M ( Lb )/GeV , 'M(LB)'  , 5.55 , 5.7 , 150 )
                
            self.treatKine ( tup , Lb  , '_Lb' )
            self.treatKine ( tup , Lc  , '_Lc' )
            self.treatKine ( tup , a1  , '_a1' )
            
            self.treatPions  ( tup, Lb )
            self.treatKaons  ( tup, Lb )
            self.treatProtons( tup, Lb )
            self.treatTracks ( tup, Lb )
            
            self.tisTos ( Lb , tup , 'Lb_' , self.lines [ 'Lb' ]  ) 
            
            
            for x in self.allmasses:
                massn=self.invmassn([vars()[y] for y in x])
                name='m_'+'_'.join(x)
                tup.column_float(name,massn)
                
                
            tup.write()
            


        self.setFilterPassed ( not cands.empty() ) 
        return SUCCESS


class MyAlg2(AlgoMC) :
    
    def all_masses ( self, particle, products, others ) :
        #
        # Returns all patricle subgroups for the process
        # something ->  (particle -> products) + others.
        #
        all_particles=[particle]+products+others
        masses,up,down=[all_particles],[all_particles],[]

        for x1 in all_particles:
            for x2 in up:
                for x3 in x2:
                    tmp=x2[:]
                    tmp.remove(x3)
                    if tmp not in down: down.append(tmp)
            masses += down
            up=down
            down=[]

        n_products=len(products)
        for x in masses[:]:  
            n_intersections=len(set(x).intersection(products))
            if any( [
                    n_products==n_intersections, 
                    (particle in x) and n_intersections>0,
                    len(x)<2 
                    ] ):
                masses.remove(x)

        return masses   

    def invmassn(self, particles) :
        p4 = Gaudi.LorentzVector()
        for p in particles :
            p4 += p.momentum()
        return p4.M()
         

    def initialize ( self ) :
        
        sc = AlgoMC.initialize ( self )
        if sc.isFailure() : return sc

                   
        triggers = {}
        triggers['Lb']= {}
        triggers['D' ]= {}
            
        lines    = {}
        lines [ "Lb"  ] = {}
        lines [ "Lb" ][   'L0TOS' ] = 'L0HadronDecision'
        lines [ "Lb" ][   'L0TIS' ] = 'L0(Hadron|DiMuon|Muon|Electron|Photon)Decision'
            
        lines [ "Lb" ][ 'Hlt1TOS' ] = 'Hlt1TrackAllL0Decision'
        lines [ "Lb" ][ 'Hlt1TIS' ] = 'Hlt1(DiMuon|SingleMuon|Track).*Decision'
            
        lines [ "Lb" ][ 'Hlt2TOS' ] = 'Hlt2(Topo2|Topo3|Topo4|Charm2)BodyBBDTDecision'
        lines [ "Lb" ][ 'Hlt2TIS' ] = 'Hlt2(Charm|Topo|DiMuon|Single).*Decision'
            
        sc = self.tisTos_initialize ( triggers , lines )
        if sc.isFailure () : return sc
        

        self.dtf_chi2_fun      = DTF_CHI2NDOF ( self , True, strings('D+'))
        self.dtf_chi2_nod_fun  = DTF_CHI2NDOF ( self , True)

        self.dtf_m_fun         = DTF_FUN  ( self , M, True, strings('D+'))
        self.dtf_m_nod_fun     = DTF_FUN  ( self , M, True)
        
        self.dtf_ctau_fun      = DTF_CTAU ( self , ABSID == 'Lambda_b0', True, strings('D+'))
        self.dtf_ctau_nod_fun  = DTF_CTAU ( self , ABSID == 'Lambda_b0', True)

        self.dtf_dctau_fun     = DTF_CTAU ( self , ABSID == 'D+', True, strings('D+'))
        self.dtf_dctau_nod_fun = DTF_CTAU ( self , ABSID == 'D+', True)
        
        self.ipchi2_fun  = BPVIPCHI2 ( self )
        self.bpvdira     = BPVDIRA   ( self  )
        
        self.allmasses = self.all_masses('D',['Dpi1','Dpi2','DK'],['p','pi1','pi2'])
        
        return SUCCESS


    
        # Main function
    def analyse(self) :

        # select the decays form the input:
        ###
        ### !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        cands = self.select ( 'candidates' , '[ Beauty -> ( D+ -> K- pi+ pi+  )  ( a_1(1260)- -> p+ pi- pi- )]CC')
        cands_size=cands.size()
        
        if 0 == cands_size : return SUCCESS
        
        rc_summary   = self.get( '/Event/Rec/Summary' ).summaryData()
        odin         = self.get( '/Event/DAQ/ODIN'    )
        
        
        mcLb  = self.mcselect  ( "mcLb"  , '[Lambda_b0 ==>   (D+ ==> K- pi+ pi+)  p+  pi-  pi-]CC' )        
        mcD   = self.mcselect  ( "mcD"  ,  '[Lambda_b0 ==>  ^(D+ ==> K- pi+ pi+)  p+  pi-  pi-]CC' )            
        mcp   = self.mcselect  ( "mcp" ,   '[Lambda_b0 ==>   (D+ ==> K- pi+ pi+) ^p+  pi-  pi-]CC')
        mcpi  = self.mcselect  ( "mcpi"  , '[Lambda_b0 ==>   (D+ ==> K- pi+ pi+)  p+ ^pi- ^pi-]CC')
        
        tLb   = MCTRUTH ( mcLb  , self.mcTruth() ) if mcLb   else PNONE
        tD    = MCTRUTH ( mcD   , self.mcTruth() ) if mcD    else PNONE
        tp    = MCTRUTH ( mcp   , self.mcTruth() ) if mcp    else PNONE
        tpi   = MCTRUTH ( mcpi  , self.mcTruth() ) if mcpi   else PNONE
        
        
        tup = self.nTuple('lb2dppipi')
        for Lb in cands :
            
            D    = Lb.child(1)
            a1   = Lb.child(2)
            
            DK   = D.child(1)
            Dpi1 = D.child(2)
            Dpi2 = D.child(3)
            
            p    = a1.child(1)
            pi1  = a1.child(2)
            pi2  = a1.child(3)
            
            ## add some reco-summary information 
            self.addRecSummary ( tup , rc_summary   )
            
            ## ODIN
            tup.column_aux    ( odin )
            
            tup.column_float ('ipchi2_Lb'    , self.ipchi2_fun ( Lb   )  ) 
            tup.column_float ('ipchi2_Dpi1'  , self.ipchi2_fun ( Dpi1 )  ) 
            tup.column_float ('ipchi2_Dpi2'  , self.ipchi2_fun ( Dpi2 )  ) 
            tup.column_float ('ipchi2_DK'    , self.ipchi2_fun ( DK   )  )
            tup.column_float ('ipchi2_p'     , self.ipchi2_fun ( p    )  ) 
            tup.column_float ('ipchi2_pi1'   , self.ipchi2_fun ( pi1  )  ) 
            tup.column_float ('ipchi2_pi2'   , self.ipchi2_fun ( pi2  )  )
            
            tup.column_float ( 'dtf_m'       , self.dtf_m_fun(Lb)        )
            tup.column_float ( 'dtf_m_nod'   , self.dtf_m_nod_fun(Lb)    )
            
            tup.column_float ( 'dtf_chi2'    , self.dtf_chi2_fun(Lb)     )
            tup.column_float ( 'dtf_chi2_nod', self.dtf_chi2_nod_fun(Lb) )
            
            tup.column_float ( 'ctau'        , self.dtf_ctau_fun(Lb))
            tup.column_float ( 'ctau_nod'    , self.dtf_ctau_nod_fun(Lb) )
            
            tup.column_float ( 'd_ctau'      , self.dtf_dctau_fun(Lb)    )
            tup.column_float ( 'd_ctau_nod'  , self.dtf_dctau_nod_fun(Lb))
            
            tup.column_float ( 'dira'          , self.bpvdira ( Lb )     )        
            tup.column_float ( 'd_dira'        , self.bpddira ( D  )     )
            
            tup.column_int   ( 'cands_size'  , int(cands_size)           )
            
            true_Lb = tLb ( Lb )
            true_D  = tD  ( D  )
            true_P  = tp  ( p  )
            true_PI = tpi ( pi1 ) and tpi ( pi2 ) 
            
            tup.column_bool ( 'mcLb'   , true_Lb )
            tup.column_bool ( 'mcD'    , true_D  )
            tup.column_bool ( 'mcp'    , true_P  )
            tup.column_bool ( 'mcpi1'  , tpi  ( pi1 ) )
            tup.column_bool ( 'mcpi2'  , tpi  ( pi2 ) )
            
            if true_Lb  :
                self.decisions ( Lb , self.triggers['Lb'] )
                self.decisions ( D ,  self.triggers['D' ] )
                
            if true_Lb and true_D and true_P and true_PI :
                self.plot( M ( Lb )/GeV , 'M(LB)'  , 5.55 , 5.7 , 150 )
                
            self.treatKine ( tup , Lb  , '_Lb' )
            self.treatKine ( tup , D   , '_D'  )
            self.treatKine ( tup , a1  , '_a1' )
            
            self.treatPions  ( tup, Lb )
            self.treatKaons  ( tup, Lb )
            self.treatProtons( tup, Lb )
            self.treatTracks ( tup, Lb )
            
            self.tisTos ( Lb , tup , 'Lb_' , self.lines [ 'Lb' ]  ) 
            
            
            for x in self.allmasses:
                massn=self.invmassn([vars()[y] for y in x])
                name='m_'+'_'.join(x)
                tup.column_float(name,massn)
                
            tup.write()
                

        self.setFilterPassed ( not cands.empty() ) 
        return SUCCESS

# =============================================================================
## configure the job
def configure ( datafiles , catalogs  = [] , castor = False,  params   = {}  ) :
    
    from Configurables import DaVinci
    from BenderTools.Parser import theYear, hasInFile 
    the_year  = theYear ( datafiles , params , '2012' )
    
    logger.info ( 'Use the Year = %s ' % the_year )
    
    # =========================================================================
    #    Re-run stripping , removing all PID cuts 
    # =========================================================================
    
    from StrippingConf.StrippingStream import StrippingStream
    from StrippingConf.Configuration import StrippingConf
    from StrippingSelections.StrippingB2OC.StrippingBeauty2Charm import Beauty2CharmConf
    from StrippingSelections.StrippingB2OC.StrippingBeauty2Charm import default_config
    
    config = default_config['CONFIG']
    
    config['HHH']['KDAUGHTERS' ]['PIDK_MIN']  = -1e30
    config['HHH']['PiDAUGHTERS']['PIDK_MAX']  = +1e30
    config['HHH']['pDAUGHTERS' ]['PIDp_MIN']  = -1e30
    
    config['PID']['P' ]['PIDp_MIN']  = -1e30
    config['PID']['PI']['PIDK_MAX']  =  1e30
    config['PID']['K' ]['PIDK_MIN']  = -1e30
    
    config['PID']['TIGHT']['P' ]['PIDp_MIN']  = -1e30
    config['PID']['TIGHT']['PI']['PIDK_MAX']  = +1e30
    config['PID']['TIGHT']['K' ]['PIDK_MIN']  = -1e30
    
    config['PID']['TIGHTER']['P' ]['PIDp_MIN']  = -1e30
    config['PID']['TIGHTER']['PI']['PIDK_MAX']  = +1e30
    config['PID']['TIGHTER']['K' ]['PIDK_MIN']  = -1e30
    
    config['PID']['TIGHTPI']['P' ]['PIDp_MIN']  = -1e30
    config['PID']['TIGHTPI']['PI']['PIDK_MAX']  = +1e30
    config['PID']['TIGHTPI']['K' ]['PIDK_MIN']  = -1e30
 
    config['PID']['TIGHTER1']['P' ]['PIDp_MIN']  = -1e30
    config['PID']['TIGHTER1']['PI']['PIDK_MAX']  = +1e30
    config['PID']['TIGHTER1']['K' ]['PIDK_MIN']  = -1e30
    
    config['PID']['TIGHTER2']['P' ]['PIDp_MIN']  = -1e30
    config['PID']['TIGHTER2']['PI']['PIDK_MAX']  = +1e30
    config['PID']['TIGHTER2']['K' ]['PIDK_MIN']  = -1e30
      
    config['PID']['SPECIAL']['P' ]['PIDp_MIN']  = -1e30
    config['PID']['SPECIAL']['PI']['PIDK_MAX']  = +1e30
    config['PID']['SPECIAL']['K' ]['PIDK_MIN']  = -1e30
   
    config['PID']['SPECIALPI']['P' ]['PIDp_MIN']  = -1e30
    config['PID']['SPECIALPI']['PI']['PIDK_MAX']  = +1e30
    config['PID']['SPECIALPI']['K' ]['PIDK_MIN']  = -1e30
    
    config['HHH']['PID']['REALTIGHTK']['P'  ]['PIDp_MIN']  = -1e30
    config['HHH']['PID']['REALTIGHTK']['PI' ]['PIDK_MAX']  = +1e30
    config['HHH']['PID']['REALTIGHTK']['K'  ]['PIDK_MIN']  = -1e30
 
    config['HHH']['PID']['TIGHTERPI']['P'  ]['PIDp_MIN']  = -1e30
    config['HHH']['PID']['TIGHTERPI']['PI' ]['PIDK_MAX']  = +1e30
    config['HHH']['PID']['TIGHTERPI']['K'  ]['PIDK_MIN']  = -1e30
 
    config['PIDPROTON']['PIDp_MIN']  = -1e30
    config['PIDPION'  ]['PIDK_MAX']  = +1e30
    config['PIDKAON'  ]['PIDK_MIN']  = -1e30

    
    stream = StrippingStream('B2Charm')
    
    restrip = [ 'StrippingLb2LcPiPiPiLc2PKPiBeauty2CharmLine' ,
                'StrippingLb2DpPiPiD2HHHBeauty2CharmLine'     ]
    
    lines = Beauty2CharmConf("Beauty2Charm", config).lines()

    for line in lines:
        if line.name() in restrip :
            stream.appendLines([line])
    
    from Configurables import EventNodeKiller
    event_node_killer = EventNodeKiller('StripKiller')
    event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

    from Configurables import ProcStatusCheck
    filterBadEvents = ProcStatusCheck()

    sc = StrippingConf(Streams=[stream],
        AcceptBadEvents=False, 
        BadEventSelection=filterBadEvents, 
        MaxCandidates = 2000 )

    from Configurables import StrippingReport
    sr = StrippingReport(Selections = sc.selections())
    sr.OnlyPositive = False
 

    daVinci = DaVinci (
        DataType         = the_year                    ,
        Simulation       = True                        ,
        InputType        = 'DST'                       , 
        Lumi             = False                       , 
        HistogramFile    = 'MyHistos_Lb2Lcpipipi.root'             , 
        TupleFile        = 'MyTuple_Lb2Lcpipipi.root' 
        )
    
    DaVinci().appendToMainSequence([event_node_killer, sc.sequence(),sr])

    from PhysSelPython.Selections import EventSelection
    from Configurables            import LoKi__MCFilter
    mc1 = EventSelection (
        'MCTrueLb1' ,
        Algorithm = LoKi__MCFilter(
        'MCTrueLb1alg' ,
        Code      = " has ( MCDECTREE ( '[Lambda_b0 ==> ( Lambda_c+ ==> p+ K- pi+ ) pi+ pi- pi-]CC') )  " ,
        Preambulo = [ 'from LoKiMC.decorators import  * ' , 'from LoKiCore.functions import has' ]  )
        )
    mc2 = EventSelection (
        'MCTrueLb2' ,
        Algorithm = LoKi__MCFilter(
        'MCTrueLb2alg' ,
        Code      = " has ( MCDECTREE ( '[Lambda_b0 ==> ( D+ ==> K- pi+ pi+ ) p+ pi- pi- ]CC') )  " ,
        Preambulo = [ 'from LoKiMC.decorators import  * ' , 'from LoKiCore.functions import has' ] )
        )
    
    from PhysConf.Selections import   AutomaticData
    the_line1 = AutomaticData ( '/Event/Phys/Lb2LcPiPiPiLc2PKPiBeauty2CharmLine/Particles' )
    the_line2 = AutomaticData ( '/Event/Phys/Lb2DpPiPiD2HHHBeauty2CharmLine/Particles' )
    
    bsel1 = BenderMCSelection (
        'lb2lcpipipi' ,
        [ mc1 , the_line1 ] , 
        PP2MCs = [ 'Relations/Rec/ProtoP/Charged' ]
        )
    
    bsel1mc = BenderMCSelection   ( 'lb2lcpipipi_MC' , mc1 , PP2MCs = [] )
    
    daVinci.UserAlgorithms.append ( bsel1   ) 
    daVinci.UserAlgorithms.append ( bsel1mc ) 
    
    bsel2 = BenderMCSelection (
        'lb2dppipi'    ,
        [ mc2 , the_line2 ] , 
        PP2MCs = [ 'Relations/Rec/ProtoP/Charged' ]
        )
    
    bsel2mc = BenderMCSelection ( 'lb2dppipi_MC', mc2 , PP2MCs = [] )
    
    daVinci.UserAlgorithms.append ( bsel2   ) 
    daVinci.UserAlgorithms.append ( bsel2mc ) 
    
    setData ( datafiles , catalogs , castor, useDBtags = True ) 


    # ========================================================================
    ## Dynamic Configuration: Jump into the wonderful world of GaudiPython 
    # =========================================================================
    
    ## get the actual application manager (create if needed)
    gaudi = appMgr() 
    
    ## create local algorithms:
    
    alg1   = MyAlg1   ( bsel1   )
    alg1mc = MyAlgMC1 ( bsel1mc )
    
    alg2   = MyAlg2   ( bsel2   )
    alg2mc = MyAlgMC2 ( bsel2mc )

    return SUCCESS 

# =============================================================================
## job steering 
if __name__ == '__main__' :

    ##'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000004_5.AllStreams.dst',

    ## configure the job:
    Lc3pi_data = [
        ## produced by the script get-files-from-BK:
        ## get-files-from-BK /MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15266055/ALLSTREAMS.DST -g CERN -a True -m 20
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000004_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000009_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000016_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000021_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000022_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000026_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000035_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000047_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000052_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000060_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000064_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000065_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000071_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000087_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000096_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000124_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000133_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000143_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000149_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069066/0000/00069066_00000154_5.AllStreams.dst']
    
    ## configure the job:
    Dp2pi_data = [
        ## produced by the script get-files-from-BK:
        ## get-files-from-BK /MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15166030/ALLSTREAMS.DST -g CERN -a True -m 20
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000009_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000029_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000049_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000050_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000072_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000095_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000096_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000098_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000106_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000107_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000122_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000127_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000128_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000152_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000155_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000160_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000167_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000174_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000175_5.AllStreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00069028/0000/00069028_00000177_5.AllStreams.dst']
  
    configure( Lc3pi_data , castor = True , params = { 'Year' : '2012' } )  


    N1 = 5000
    N2 = 5000
    
    ## run the job
    run ( N1 )

    setData ( Dp2pi_data , castor = True )

    run ( N2 )

    gaudi = appMgr() 
    alg1  = gaudi.algorithm( 'lb2lcpipipi' )
    alg2  = gaudi.algorithm( 'lb2dppipi'   )

    cnt1  = alg1.Counters('#accept')
    cnt2  = alg2.Counters('#accept')

    from LHCbMath.Types import VE, cpp
    BE = cpp.Gaudi.Math.binomEff

    e1 = BE ( int ( cnt1.sum().value() ) , N1 ) * 100
    e2 = BE ( int ( cnt2.sum().value() ) , N2 ) * 100
    
    logger.info ( 'Efficiency (Lc3pi) %s%%' % e1 )
    logger.info ( 'Efficiency (Dp2pi) %s%%' % e2 )

    if (e1/e2).value() < 0.5  or (e2/e1).value() < 0.5  :
        raise RuntimeError('Efficiencies are very different %s%% : %s%%' % ( e1 , e2 ) )


# =============================================================================
##                                                                      The END 
# =============================================================================
