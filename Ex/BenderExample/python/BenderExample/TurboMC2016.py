#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file TurboMC2016.py 
#
#  MC-match for Turbo/2016 simulated samples 
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly 
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2018-01-30
#
# =============================================================================
""" MC-match for Turbo/2016 simulated samples

oooooooooo.                              .o8                     
`888'   `Y8b                            \"888                     
 888     888  .ooooo.  ooo. .oo.    .oooo888   .ooooo.  oooo d8b 
 888oooo888' d88' `88b `888P\"Y88b  d88' `888  d88' `88b `888\"\"8P 
 888    `88b 888ooo888  888   888  888   888  888ooo888  888     
 888    .88P 888    .o  888   888  888   888  888    .o  888     
o888bood8P'  `Y8bod8P' o888o o888o `Y8bod88P\" `Y8bod8P' d888b    

This file is a part of BENDER project:
   ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from
Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project:
   ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement 
with the smear campaign of Dr.O.Callot et al.: 
    ``No Vanya's lines are allowed in LHCb/Gaudi software.''

"""
# =============================================================================
__author__  = " Vanya BELYAEV Ivan.Belyaev@itep.ru "
__date__    = " 2018-01-30" 
__version__ = " $Revision$"
# ============================================================================= 
## import everything from bender 
from   Bender.MainMC             import *
from   GaudiKernel.SystemOfUnits import GeV 
# =============================================================================
## optional logging
# =============================================================================
from Bender.Logger import getLogger 
if '__main__' == __name__ : logger = getLogger ( 'BenderExample.TurboMC' )
else                      : logger = getLogger ( __name__ )
# =============================================================================
## @class TurboMC
#  Reading TURBO/2015 MC  with Bender 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2015-10-31
class TurboMC(AlgoMC):
    """Reading TURBO/2015 MC with Bender 
    """
    ## the main 'analysis' method 
    def analyse( self ) :   ## IMPORTANT! 
        """The main 'analysis' method
        """
        
        ## get particles from the input locations 
        particles = self.select ( 'dstar', "[D*(2010)+ -> ( D0 -> K- pi+ ) pi+]CC" )
        if not particles :
            return self.Warning('No good reconstructed D*+ are found' , SUCCESS )

        
        mccharm   = self.mcselect( 'mc'   , "[D*(2010)+ => ( D0 =>  K-   pi+ )  pi+]CC" )
        if not mccharm : return self.Warning('No true D*+ are found' , SUCCESS )
        
        mck        = self.mcselect( 'mcK'  , "[D*(2010)+ => ( D0 => ^K-   pi+ )  pi+]CC" )
        mcpi       = self.mcselect( 'mcpi' , "[D*(2010)+ => ( D0 =>  K-  ^pi+ )  pi+]CC" )
        mcs        = self.mcselect( 'mcs' , "[D*(2010)+ => ( D0 =>  K-   pi+ ) ^pi+]CC" )

        true_charm = MCTRUTH ( self.mcTruth() , mccharm  )
        true_k     = MCTRUTH ( self.mcTruth() , mck      )
        true_pi    = MCTRUTH ( self.mcTruth() , mcpi     )
        true_s     = MCTRUTH ( self.mcTruth() , mcs      )
        
        c1 = self.counter ( 'True D*'      )        
        c2 = self.counter ( 'Fake K'       )
        c3 = self.counter ( 'Fake pi+'     )
        c4 = self.counter ( 'Fake pi+slow' )

        locs = set()
        
        for p in particles :

            tc = true_charm  ( p  )
            c1 += tc  
            if tc  : self.Info( 'TRUE :\n%s' % p ) 
            else   :
                
                D0      = p (1)
                good_k  = true_k ( D0 ( 1 ) )
                good_pi = true_k ( D0 ( 2 ) )
                good_s  = true_s ( p  ( 2 ) )

                if not good_k  : c2 += 1
                if not good_pi : c3 += 1
                if not good_s  : c4 += 1
                self.Info( 'FAKE (K:%s pi+:%s) pi+slow:%s' % ( good_k , good_pi , good_s ) )
                           
        ##
        
        return SUCCESS      ## IMPORTANT!!! 
# =============================================================================

# =============================================================================
## The configuration of the job
def configure ( inputdata        ,    ## the list of input files  
                catalogs = []    ,    ## xml-catalogs (filled by GRID)
                castor   = False ,    ## use the direct access to castor/EOS ? 
                params   = {}    ) :
    
    the_year = params.get('Year', '2016')
    
    ## delegate the actual configuration to DaVinci
    hlt2_line   = 'Hlt2PIDD02KPiTagTurboCalib'

    from Configurables import DaVinci
    dv = DaVinci ( DataType        = the_year           ,
                   InputType       = 'MDST'             , ## ATTENTION!
                   RootInTES       = '/Event/Turbo'     , ## ATTENTION! 
                   Turbo           = True               , ## ATTENTION! 
                   Simulation      = True               , ## ATTENTION! 
                   TupleFile       = 'TurboMC2016.root' , ## IMPORTANT
                   )
    
    from PhysConf.Selections import AutomaticData
    charm = AutomaticData ( "%s/Particles" % hlt2_line , monitor = True )
    
    ##from PhysConf.Selections import PrintSelection
    ##charm = PrintSelection ( charm )
    
    ## Bender selections
    bsel = BenderMCSelection (
        'TurboMC' ,
        charm     , 
        PP2MCs = [ 'Relations/Turbo/Protos' ] , ##  ATTENTION! 
        )
    
    ## add them to DaVinci 
    dv.UserAlgorithms.append ( bsel )

    ## define the input data
    setData  ( inputdata , catalogs , castor , useDBtags = True )
    
    ## get/create application manager
    gaudi = appMgr() 

    ## create the algorithms 
    alg   = TurboMC ( bsel ) 
    
    return SUCCESS 
# =============================================================================

# =============================================================================
## Job steering 
if __name__ == '__main__' :

    logger.info ( 80*'*'  ) 
    logger.info ( __doc__ ) 
    logger.info ( ' Author  : %s ' %  __author__  ) 
    logger.info ( ' Version : %s ' %  __version__ ) 
    logger.info ( ' Date    : %s ' %  __date__    ) 
    logger.info ( 80*'*'  ) 

    #
    ## job configuration
    #
    inputdata = [
        '/lhcb/MC/2016/DST/00066623/0000/00066623_00000001_1.dst'
        ]
    configure( inputdata , castor = True , params = { 'Year' : '2016' } )
    
    ## event loop
    run(1000)
    
    
# =============================================================================
# The END
# =============================================================================


