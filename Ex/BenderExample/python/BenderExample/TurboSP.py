#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file TurboSP.py 
#
#  Reading TURBO/SelectivePersistency with Bender 
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly 
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2018-01-30
#
# =============================================================================
""" Reading TURBO with Bedner 

oooooooooo.                              .o8                     
`888'   `Y8b                            \"888                     
 888     888  .ooooo.  ooo. .oo.    .oooo888   .ooooo.  oooo d8b 
 888oooo888' d88' `88b `888P\"Y88b  d88' `888  d88' `88b `888\"\"8P 
 888    `88b 888ooo888  888   888  888   888  888ooo888  888     
 888    .88P 888    .o  888   888  888   888  888    .o  888     
o888bood8P'  `Y8bod8P' o888o o888o `Y8bod88P\" `Y8bod8P' d888b    

This file is a part of BENDER project:
   ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from
Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project:
   ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement 
with the smear campaign of Dr.O.Callot et al.: 
    ``No Vanya's lines are allowed in LHCb/Gaudi software.''

"""
# =============================================================================
__author__  = " Vanya BELYAEV Ivan.Belyaev@itep.ru "
__date__    = " 2018-01-30" 
__version__ = " $Revision$"
# ============================================================================= 
## import everything from bender 
from   Bender.Main               import *
from   GaudiKernel.SystemOfUnits import GeV 
# =============================================================================
## optional logging
# =============================================================================
from Bender.Logger import getLogger 
if '__main__' == __name__ : logger = getLogger ( 'BenderExample.TurboSP' )
else                      : logger = getLogger ( __name__ )
# =============================================================================
## @class TurboSP
#  Reading TURBO/SP with Bender 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2015-10-31
class TurboSP(Algo):
    """Reading TURBO/SP with Bender 
    """
    ## the main 'analysis' method 
    def analyse( self ) :   ## IMPORTANT! 
        """
        The main 'analysis' method
        """
        
        ## get particles from the input locations 
        particles = self.select ( 'all', ALL )
        locs = set() 
        for p in particles : 
            for t in p.tracks() :
                tl = t.location()
                tl = tl[:tl.find('[')]
                locs.add ( tl )
                
        self.Print( "Track locations: %s" % list ( locs ) ) 
        
        
        ## 
        return SUCCESS      ## IMPORTANT!!! 
# =============================================================================

# =============================================================================
## The configuration of the job
def configure ( inputdata        ,    ## the list of input files  
                catalogs = []    ,    ## xml-catalogs (filled by GRID)
                castor   = False ,    ## use the direct access to castor/EOS ? 
                params   = {}    ) :
    
    the_year = params.get('Year', '2017')
    
    ## delegate the actual configuration to DaVinci
    rootInTES   = '/Event/Charmspec/Turbo'
    
    hlt2_line   = 'Hlt2CharmHadD02KmPipTurbo'
    
    extra1      = 'DstD0eeExtraSel'
    extra2      = 'DstD0GammaExtraSel'
    
    from Configurables import DaVinci
    dv = DaVinci ( DataType        = the_year                ,
                   InputType       = 'MDST'                  ,
                   Turbo           = True                    ,
                   Lumi            = True                    ,
                   RootInTES       = rootInTES               ,
                   TupleFile       = 'Turbo.root'        ,  ## IMPORTANT 
                   )
    
    from PhysConf.Selections import AutomaticData
    charm       = AutomaticData ( "%s/Particles"    %   hlt2_line )
    
    from PhysConf.Selections import MomentumScaling
    charm = MomentumScaling ( charm , Turbo = True , Year = the_year )

    ## get Selective Persistency 
    extra_ee    = AutomaticData ( "%s/%s/Particles" % ( hlt2_line , extra1 ) ) 
    extra_gamma = AutomaticData ( "%s/%s/Particles" % ( hlt2_line , extra2 ) ) 
    
    ##from PhysConf.Selections import PrintSelection
    ##charm = PrintSelection  ( charm )

    ##  get persist reco 
    from StandardParticles   import StdAllNoPIDsMuons as muons
    from PhysConf.Selections import RebuildSelection
    muons = RebuildSelection ( muons )

    from PhysConf.Selections import PrescaleEventSelection
    prescale = PrescaleEventSelection ( 0.02 ) 

    ## Bender selections
    b1 = BenderSelection ( 'B_gamma' , [ extra_gamma , charm ] )
    b2 = BenderSelection ( 'B_ee'    , [ extra_ee    , charm ] )
    
    b3 = BenderSelection ( 'B_charm' , [ prescale , charm ]   )    
    b4 = BenderSelection ( 'B_muons' , [ prescale , charm , muons ]   )    

    ## add them to DaVinci 
    dv.UserAlgorithms.append ( b1 )
    dv.UserAlgorithms.append ( b2 )
    dv.UserAlgorithms.append ( b3 )
    dv.UserAlgorithms.append ( b4 )

    # =========================================================================
    from Configurables import CondDB
    CondDB ( LatestGlobalTagByDataType = the_year )
    # =========================================================================

    ## define the input data
    setData  ( inputdata , catalogs , castor )
    
    ## get/create application manager
    gaudi = appMgr() 

    ## create the algorithms 
    alg1 = TurboSP ( b1 ) 
    alg2 = TurboSP ( b2 ) 
    alg3 = TurboSP ( b3 ) 
    alg4 = TurboSP ( b4 ) 
    
    return SUCCESS 
# =============================================================================

# =============================================================================
## Job steering 
if __name__ == '__main__' :

    logger.info ( 80*'*'  ) 
    logger.info ( __doc__ ) 
    logger.info ( ' Author  : %s ' %  __author__  ) 
    logger.info ( ' Version : %s ' %  __version__ ) 
    logger.info ( ' Date    : %s ' %  __date__    ) 
    logger.info ( 80*'*'  ) 

    #
    ## job configuration
    #
    inputdata = [
        '/lhcb/LHCb/Collision17/CHARMSPEC.MDST/00064380/0000/00064380_00000003_1.charmspec.mdst',
        '/lhcb/LHCb/Collision17/CHARMSPEC.MDST/00064380/0000/00064380_00000013_1.charmspec.mdst'
        ]
    configure( inputdata , castor = True , params = { 'Year' : '2017' } )
    
    ## event loop
    run(10000)
    
    
# =============================================================================
# The END
# =============================================================================


