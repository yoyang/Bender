#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file Turbo_2018.py 
#
#  Reading TURBO-05/2018 with Bedner 
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly 
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2017-01-20
#
# =============================================================================
""" Reading TURBO with Bedner 

oooooooooo.                              .o8                     
`888'   `Y8b                            \"888                     
 888     888  .ooooo.  ooo. .oo.    .oooo888   .ooooo.  oooo d8b 
 888oooo888' d88' `88b `888P\"Y88b  d88' `888  d88' `88b `888\"\"8P 
 888    `88b 888ooo888  888   888  888   888  888ooo888  888     
 888    .88P 888    .o  888   888  888   888  888    .o  888     
o888bood8P'  `Y8bod8P' o888o o888o `Y8bod88P\" `Y8bod8P' d888b    

This file is a part of BENDER project:
   ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from
Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project:
   ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement 
with the smear campaign of Dr.O.Callot et al.: 
    ``No Vanya's lines are allowed in LHCb/Gaudi software.''

"""
# =============================================================================
__author__  = " Vanya BELYAEV Ivan.Belyaev@itep.ru "
__date__    = " 2017-01-20" 
__version__ = " $Revision$"
# ============================================================================= 
## import everything from bender 
from   Bender.Main               import *
from   GaudiKernel.SystemOfUnits import GeV 
# =============================================================================
## optional logging
# =============================================================================
from Bender.Logger import getLogger 
if '__main__' == __name__ : logger = getLogger ( 'BenderExample.Turbo' )
else                      : logger = getLogger ( __name__ )
# =============================================================================
from LoKiCore.functions import strings 
# =============================================================================
## @class Turbo
#  Reading TURBO with Bender 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2015-10-31
class Turbo(Algo):
    """Reading TURBO with Bender 
    """
    ## the main 'analysis' method 
    def analyse( self ) :   ## IMPORTANT! 
        """
        The main 'analysis' method
        """
        
        ## get particles from the input locations 
        particles = self.select ( 'charm', '[D*(2010)+ -> ( D0 -> K- pi+  ) pi+ ]CC'  )
        if particles.empty() :
            return self.Warning('No good input D*+ mesons', SUCCESS )
        
        tup = self.nTuple('MyTuple')
        
        fun_mass_1  = DTF_FUN      ( self , M  ,             True ) 
        fun_mass_2  = DTF_FUN      ( self , M  ,             True , 'D0' )
        
        fun_mass1   = DTF_FUN      ( self , M1 ,             True )

                                   
        fun_chi2_1 = DTF_CHI2NDOF ( self ,                 True )
        fun_chi2_2 = DTF_CHI2NDOF ( self ,                 True , 'D0' ) 


        fun_ctau1_1 = DTF_CTAU     ( self , 1 , True ) 
        fun_ctau1_2 = DTF_CTAU     ( self , 1 , True , strings ( ['D0'] ) )  

        for d in particles :

            chi2 = fun_chi2_1 ( d )
            if not 0<= chi2 < 10 : continue
            
            ctau1 = fun_ctau1_1 ( d )
            ctau2 = fun_ctau1_1 ( d )

            ## M & M1: no constraints 
            tup.column (  'm'       , M  ( d ) / GeV )
            tup.column ( 'm1'       , M1 ( d ) / GeV )

            ## M with refit and PV, M1 constraints:
            tup.column ( 'm_dtf_2'  , fun_mass_2 ( d ) / GeV )
            
            ## M & M1  with refit and PV-constraints: 
            tup.column ( 'm_dtf_1'  , fun_mass_1 ( d ) / GeV )
            tup.column ( 'm1_dtf_1' , fun_mass1  ( d ) / GeV )
            
            tup.column ( 'ctau1_1' , ctau1        ) 
            tup.column ( 'ctau1_2' , ctau2        ) 
            
            tup.column ( 'chi2_1'  , chi2  ) 
            tup.column ( 'chi2_2'  , fun_chi2_2 ( d )  )
            
            self.treatKine    ( tup , d    , '_Dst' )
            self.treatKine    ( tup , d(1) , '_D0'  )
            self.treatKine    ( tup , d(2) , '_pi'  )
            
            self.treatPions   ( tup , d )
            self.treatKaons   ( tup , d )
            self.treatTracks  ( tup , d )

            tup.write() 
            
        ## 
        return SUCCESS      ## IMPORTANT!!! 
# =============================================================================

# =============================================================================
## The configuration of the job
def configure ( inputdata        ,    ## the list of input files  
                catalogs = []    ,    ## xml-catalogs (filled by GRID)
                castor   = False ,    ## use the direct access to castor/EOS ? 
                params   = {}    ) :
    
    ## import DaVinci 
    from Configurables import DaVinci
    ## delegate the actual configuration to DaVinci
    rootInTES = '/Event/Charmtwobody/Turbo'
    the_line  = 'Hlt2CharmHadDstp2D0Pip_D02KmPipTurbo/Particles'
    the_year  = params['Year']
    
    dv = DaVinci ( DataType        = the_year                ,
                   InputType       = 'MDST'                  ,
                   Turbo           = True                    ,
                   RootInTES       = rootInTES               ,
                   TupleFile       = 'Turbo.root'        ,  ## IMPORTANT 
                   )
        
    from PhysConf.Selections import AutomaticData
    input  = AutomaticData ( the_line , monitor = True )
    
    ## from PhysConf.Selections import PrintSelection
    ## input  = PrintSelection ( input )
        
    from Configurables import CondDB
    CondDB ( LatestGlobalTagByDataType = the_year )
    # =========================================================================

    ## Bender-selection - wrap Bender algorithm as ``Selection''
    bsel   = BenderSelection   ( 'Turbo', input )

    ## add Selection sequence into DaVinci dataflow 
    dv.UserAlgorithms.append ( bsel )
    
    ## define the input data
    setData  ( inputdata , catalogs , castor )
    
    ## get/create application manager
    gaudi = appMgr() 
    
    ## (1) create the algorithm with given name 
    alg   = Turbo ( bsel )
             
    return SUCCESS 
# =============================================================================

# =============================================================================
## Job steering 
if __name__ == '__main__' :

    logger.info ( 80*'*'  ) 
    logger.info ( __doc__ ) 
    logger.info ( ' Author  : %s ' %  __author__  ) 
    logger.info ( ' Version : %s ' %  __version__ ) 
    logger.info ( ' Date    : %s ' %  __date__    ) 
    logger.info ( 80*'*'  ) 
    
    ## produced by the script get-files-from-BK:
    ## get-files-from-BK '/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo05/94000000/CHARMTWOBODY.MDST' -g CERN -m 10
    inputdata = [
        '/lhcb/LHCb/Collision18/CHARMTWOBODY.MDST/00075136/0000/00075136_00000005_1.charmtwobody.mdst',
        '/lhcb/LHCb/Collision18/CHARMTWOBODY.MDST/00075136/0000/00075136_00000012_1.charmtwobody.mdst',
        '/lhcb/LHCb/Collision18/CHARMTWOBODY.MDST/00075136/0000/00075136_00000026_1.charmtwobody.mdst',
        '/lhcb/LHCb/Collision18/CHARMTWOBODY.MDST/00075136/0000/00075136_00000033_1.charmtwobody.mdst',
        '/lhcb/LHCb/Collision18/CHARMTWOBODY.MDST/00075136/0000/00075136_00000040_1.charmtwobody.mdst',
        '/lhcb/LHCb/Collision18/CHARMTWOBODY.MDST/00075136/0000/00075136_00000054_1.charmtwobody.mdst',
        '/lhcb/LHCb/Collision18/CHARMTWOBODY.MDST/00075136/0000/00075136_00000075_1.charmtwobody.mdst',
        '/lhcb/LHCb/Collision18/CHARMTWOBODY.MDST/00075136/0000/00075136_00000082_1.charmtwobody.mdst',
        '/lhcb/LHCb/Collision18/CHARMTWOBODY.MDST/00075136/0000/00075136_00000089_1.charmtwobody.mdst',
        '/lhcb/LHCb/Collision18/CHARMTWOBODY.MDST/00075136/0000/00075136_00000103_1.charmtwobody.mdst'
        ]
    
    #
    ## job configuration
    #

    configure( inputdata , castor = True , params = { 'Year' : '2018' } )
    
    ## event loop 
    run(1000)
    
    gaudi = appMgr()
    alg   = gaudi.algorithm('Turbo')
    alg.NTuplePrint = True 
                          
    
# =============================================================================
# The END
# =============================================================================


