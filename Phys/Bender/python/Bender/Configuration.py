#!/usr/bin/env python 
# -*- coding: utf-8 -*-
# =============================================================================
# $Id: Configuration.py$ 
# =============================================================================
## @file Bender/Configuration.py
#  The helper Python module for Bender application 
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly 
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @date   2004-07-11
#  @author Vanya BELYAEV ibelyaev@physics.syr.edu
#
#                    $Revision: 206755 $
#  Last modification $Date: 2016-06-08 17:01:05 +0200 (Wed, 08 Jun 2016) $
#                 by $Author: ibelyaev $ 
# =============================================================================
"""This is a helper Python Module for Bender application

oooooooooo.                              .o8                     
`888'   `Y8b                            \"888                     
 888     888  .ooooo.  ooo. .oo.    .oooo888   .ooooo.  oooo d8b 
 888oooo888' d88' `88b `888P\"Y88b  d88' `888  d88' `88b `888\"\"8P 
 888    `88b 888ooo888  888   888  888   888  888ooo888  888     
 888    .88P 888    .o  888   888  888   888  888    .o  888     
o888bood8P'  `Y8bod8P' o888o o888o `Y8bod88P\" `Y8bod8P' d888b    

This file is a part of BENDER project:
    ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project:
    ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement 
with the smear campain of Dr.O.Callot et al.: 
    ``No Vanya's lines are allowed in LHCb/Gaudi software.''

  Last modification $Date: 2016-06-08 17:01:05 +0200 (Wed, 08 Jun 2016) $
                 by $Author: ibelyaev $ 
"""
# =============================================================================
__author__  = 'Vanya BELYAEV ibelyaev@physics.syr.edu'
__date__    = "2016-06-09"
__version__ = '$Revision: 206755 $'
__all__     = (
    'BenderAlgo'      ,  ## pseudo-configurbale for Bender.Algo 
    'BenderAlgoMC'    ,  ## pseudo-configurable for Bender.AlgoMC 
    'PythonAlgorithm' ,  ## ``fake configurable''
    ) 
# =============================================================================
## logging
# =============================================================================
from Bender.Logger import getLogger 
if '__main__' == __name__ : logger = getLogger ( 'Bender.Configuration' )
else                      : logger = getLogger ( __name__ )
# =============================================================================
## make fake configurable
# =============================================================================
from GaudiKernel.Configurable import ConfigurableAlgorithm
# =============================================================================
## @class PythonAlgorithm
#  simple ``fake'' Configurable for python/Bender algorithm
#  @code
#  phi                 = PythonAlgorithm('Phi')
#  phi.HistoPrint      = True
#  phi.PropertiesPrint = True
#  phi.Inputs          = InputParticles
#  seq.Memebers += [ phi ]
#  @endcode  
class PythonAlgorithm(ConfigurableAlgorithm):
    """Simple ``fake'' Configurable for python/Bender algorithm
    
    phi                 = PythonAlgorithm('Phi')
    phi.HistoPrint      = True
    phi.PropertiesPrint = True
    phi.Inputs          = InputParticles
    seq.Memebers += [ phi ]
    """
    def __init__( self , name , **kwargs):
        super(PythonAlgorithm, self).__init__(name)
        for n,v in kwargs.items():
            setattr(self, n, v)

    def getDlls           ( self ) : return ''
    def name              ( self ) : return self.getName() 
    def getFullName       ( self ) : return self.getName() 
    def getFullJobOptName ( self ) : return self.getName()
    def getPrintTitle(self):
        return "PythonAlgorithm ('%s')"  %  self.getTitleName()


# =============================================================================
try :
    
    from Configurables         import LoKi__Algo
    if not  LoKi__Algo :
        raise ImportError,"Invalid LoKi::Algo"
    # =========================================================================
    ## @class BenderAlgo
    #  Simple ``fake'' Configurable for python/Bender algorithm
    class BenderAlgo(ConfigurableAlgorithm):
        """Simple ``fake'' Configurable for python/Bender algorithm
        
        phi                 = BenderAlgo('Phi')
        phi.HistoPrint      = True
        phi.PropertiesPrint = True
        phi.Inputs          = InputParticles
        seq.Members += [ phi ]
        """
        ## copy configuration from C++ algorithm 
        __slots__       = LoKi__Algo.__slots__
        _propertyDocDct = LoKi__Algo._propertyDocDct
        ## 
        def __init__( self , name , **kwargs):
            super(BenderAlgo, self).__init__(name)
            for n,v in kwargs.items():
                setattr(self, n, v)
                
        def getDlls           ( self ) : return ''
        def name              ( self ) : return self.getName() 
        def getFullName       ( self ) : return self.getName() 
        def getFullJobOptName ( self ) : return self.getName()
        def getPrintTitle(self):
            return "BenderAlgo('%s')"  %  self.getTitleName()
        
except ImportError :
    logger.warning ("Can't import LoKi::Algo configuration") 
    BenderAlgo = PythonAlgorithm


# =============================================================================
try :

    from Configurables         import LoKi__AlgoMC
    if not  LoKi__AlgoMC :
        raise ImportError,"Invalid LoKi::AlgoMC"
    # =========================================================================
    ## @class BenderAlgoMC
    #  Simple ``fake'' Configurable for python/Bender algorithm
    class BenderAlgoMC(ConfigurableAlgorithm):
        """Simple ``fake'' Configurable for python/Bender algorithm
        
        phi                 = BenderAlgoMC('Phi')
        phi.HistoPrint      = True
        phi.PropertiesPrint = True
        phi.Inputs          = InputParticles
        seq.Members += [ phi ]
        """
        ## copy configuration from C++ algorithm 
        __slots__       = LoKi__AlgoMC.__slots__
        _propertyDocDct = LoKi__AlgoMC._propertyDocDct
        ## 
        def __init__( self , name , **kwargs):
            super(BenderAlgoMC, self).__init__(name)
            for n,v in kwargs.items():
                setattr(self, n, v)
                
        def getDlls           ( self ) : return ''
        def name              ( self ) : return self.getName() 
        def getFullName       ( self ) : return self.getName() 
        def getFullJobOptName ( self ) : return self.getName()
        def getPrintTitle(self):
            return "BenderAlgo('%s')"  %  self.getTitleName()
        
except ImportError :
    logger.warning ("Can't import LoKi::AlgoMC configuration") 
    BenderAlgoMC = PythonAlgorithm


# =============================================================================
if __name__ == '__main__' :

    logger.info ( 80*'*'  ) 
    logger.info ( __doc__ ) 
    logger.info ( ' Author  : %s ' %  __author__  ) 
    logger.info ( ' Version : %s ' %  __version__ ) 
    logger.info ( ' Date    : %s ' %  __date__    ) 
    logger.info ( 80*'*'  ) 

# =============================================================================
# The END 
# =============================================================================
