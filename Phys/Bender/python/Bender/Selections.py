#!/usr/bin/env python 
# -*- coding: utf-8 -*-
# =============================================================================
## @file Bender/Selections.py
#  Simple Bender-based ``selections''
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly 
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @date   2004-07-11
#  @author Vanya BELYAEV ibelyaev@physics.syr.edu
#
# =============================================================================
"""Simple Bender-based ``selections''

oooooooooo.                              .o8                     
`888'   `Y8b                            \"888                     
 888     888  .ooooo.  ooo. .oo.    .oooo888   .ooooo.  oooo d8b 
 888oooo888' d88' `88b `888P\"Y88b  d88' `888  d88' `88b `888\"\"8P 
 888    `88b 888ooo888  888   888  888   888  888ooo888  888     
 888    .88P 888    .o  888   888  888   888  888    .o  888     
o888bood8P'  `Y8bod8P' o888o o888o `Y8bod88P\" `Y8bod8P' d888b    

This file is a part of BENDER project:
    ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project:
    ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement 
with the smear campain of Dr.O.Callot et al.: 
    ``No Vanya's lines are allowed in LHCb/Gaudi software.''
"""
# =============================================================================
__author__  = 'Vanya BELYAEV  Ivan.Belyaev@itep.ru'
__date__    = "2016-10-12"
__version__ = '$Revision$'
__all__     = (
    'BenderSelection'   , ## Bender-based ``selection''
    'BenderMCSelection' , ## Bender-based ``selection''   
    )
# =============================================================================
## logging
# =============================================================================
from Bender.Logger import getLogger 
if '__main__' == __name__ : logger = getLogger ( 'Bender.Selections' )
else                      : logger = getLogger ( __name__            )
# =============================================================================
## create very simple Bender-based ``selection''
#  (== ``Selection'' using BenderAlgo)
#  @code
#  ## Bender algorithm
#  class MyNiceAlg(Algo) : ...
#  
#  ## Configuration:
#  bsel = BenderSelection ( 'MyAlg' ,
#                           inputs = [ kaons , pions ] )
#  bsel = PrintSelection  ( bsel )
#  ...
#  alg  = MyNiceAlg('MyAlg')  
#  @endcode
#  @author = 'Vanya BELYAEV  Ivan.Belyaev@itep.ru'
#  @date   = "2016-10-12"
def BenderSelection ( name        ,
                      inputs = [] ,
                      *args       ,
                      **kwargs    ) :
    """Create very simple Bender-based ``selection''
    (== ``Selection'' using BenderAlgo)
    
    ## 1) Bender algorithm
    class MyNiceAlg(Algo) : ...
    
    ## 2) Configuration:
    
    bsel = BenderSelection ( 'MyAlg' , inputs = [ kaons , pions ] )
    bsel = PrintSelection  ( bsel )
    
    ## 3) finally, create algorithm:
    alg  = MyNiceAlg('MyAlg')
    
    """
    from PhysSelPython.Selections              import SimpleSelection
    from GaudiConfUtils.ConfigurableGenerators import BenderAlgo as BA
    return SimpleSelection ( name , BA , inputs , *args , **kwargs )

# =============================================================================
## create very simple Bender-based ``selection''
#  (== ``Selection'' using BenderAlgoMC)
#  @code
#  ## Bender algorithm
#  class MyNiceAlg(AlgoMC) : ...
#  
#  ## Configuration:
#  bsel = BenderMCSelection ( 'MyAlg' ,
#                              inputs = [ kaons , pions ] )
#  bsel = PrintSelection    ( bsel )
#  ...
#  alg  = MyNiceAlg('MyAlg')  
#  @endcode
#  @author = 'Vanya BELYAEV  Ivan.Belyaev@itep.ru'
#  @date   = "2016-10-12"
def BenderMCSelection ( name        ,
                        inputs = [] ,
                        *args       ,
                        **kwargs    ) :
    """Create very simple Bender-based ``selection''
    (== ``Selection'' using BenderAlgoMC)
    
    ## 1) Bender algorithm
    class MyNiceAlg(AlgoMC) : ...
    
    ## 2) Configuration:
    
    bsel = BenderMCSelection ( 'MyAlg' , inputs = [ kaons , pions ] )
    bsel = PrintSelection    ( bsel )
    
    ## 3) finally, create algorithm:
    alg  = MyNiceAlg('MyAlg')
    
    """
    from PhysSelPython.Selections              import SimpleSelection
    from GaudiConfUtils.ConfigurableGenerators import BenderAlgoMC    as BA
    return SimpleSelection ( name , BA , inputs , *args , **kwargs )

## =============================================================================
if __name__ == '__main__' :

    logger.info ( 80*'*'  ) 
    logger.info ( __doc__ ) 
    logger.info ( ' Author  : %s ' %  __author__  ) 
    logger.info ( ' Version : %s ' %  __version__ ) 
    logger.info ( ' Date    : %s ' %  __date__    ) 
    logger.info ( 80*'*'  ) 

# =============================================================================
# The END 
# =============================================================================
