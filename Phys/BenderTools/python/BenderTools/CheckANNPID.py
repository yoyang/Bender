#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file CheckANNPID.py
#  Simple algorithm to check version of ANNPID variables
#
#  This file is a part of
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV.
#  And it is based on the
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement
#  with the smear campaign of Dr.O.Callot et al.:
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2018-02-02
#
# =============================================================================
"""Simple algorithm to check version of ANNPID variables

oooooooooo.                              .o8
`888'   `Y8b                            \"888
 888     888  .ooooo.  ooo. .oo.    .oooo888   .ooooo.  oooo d8b
 888oooo888' d88' `88b `888P\"Y88b  d88' `888  d88' `88b `888\"\"8P
 888    `88b 888ooo888  888   888  888   888  888ooo888  888
 888    .88P 888    .o  888   888  888   888  888    .o  888
o888bood8P'  `Y8bod8P' o888o o888o `Y8bod88P\" `Y8bod8P' d888b

This file is a part of BENDER project:
   ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from
Pere MATO and Andrey TSAREGORODTSEV.

And it is based on the LoKi project:
   ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement
with the smear campaign of Dr.O.Callot et al.:
    ``No Vanya's lines are allowed in LHCb/Gaudi software.''

"""
# =============================================================================
__author__ = " Vanya BELYAEV Ivan.Belyaev@itep.ru "
__date__ = " 2018-02-02"
__version__ = " $Revision$"
__all__ = (
    'check_annpid'  , ## helper function to check ANNPID tunes
    'config_annpid' , ## configure function for  ANNPID/PROBNN check 
)
# =============================================================================
particles = ('PION', 'KAON', 'ELECTRON', 'PROTON', 'MUON', 'GHOST')
## import stuff from bender
from Bender.Main import ( Algo    , SUCCESS  ,
                          ALL     , ANNPID   , ISBASIC     , HASTRACK    ,
                          PROBNNpi, PROBNNmu , PROBNNe     , 
                          PROBNNk , PROBNNp  , PROBNNghost ) 
# =============================================================================
## logging
# =============================================================================
from Bender.Logger import getLogger
logger = getLogger( __name__ )
# =============================================================================
## @class CheckANNPID
#  Check ANNPID tune/version using Bender
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2015-10-31
class CheckANNPID(Algo):
    """Reading TURBO/SP with Bender
    """

    def initialize(self):

        sc = Algo.initialize(self)
        if sc.isFailure(): return sc

        from BenderTools.Fill import ANNPIDTUNES
        tunes = ANNPIDTUNES.known_tunes

        nu = self.name().upper()

        if   'PION'     in nu :
            fun, particle = PROBNNpi, 'PION'
        elif 'MUON'     in nu :
            fun, particle = PROBNNmu, 'MUON' 
        elif 'ELECTRON' in nu : 
            fun, particle = PROBNNe, 'ELECTRON' 
        elif 'PROTON'   in nu : 
            fun, particle = PROBNNp, 'PROTON' 
        elif 'GHOST'    in nu : 
            fun, particle = PROBNNghost, 'GHOST' 
        elif 'KAON'     in nu : 
            fun, particle = PROBNNk, 'KAON'
        elif 'PI'       in nu : 
            fun, particle = PROBNNpi, 'PION'
        elif 'MU'       in nu : 
            fun, particle = PROBNNmu, 'MUON'
        else:
            self.Warning ( "Can't deduce the particle name from ``%s'', use PION hypotheses" % self.name() )
            fun , particle = PROBNNpi, 'PION'
            
        self._probnn = fun
        
        entries = []
        for tune in tunes:
            entry = ( self.counter ( 'ANNPID_%s:%s' % ( particle , tune ) ) , 
                      ANNPID( particle , tune )                             ,
                      tune                                                  ) 
            entries.append ( entry )

        self._entries = tuple ( entries )

        return SUCCESS

    ## find the most probable tune 
    def the_tune ( self ) :

        ddmax = -1 
        tune  = ''
        cnt   = None 
        for c, f, t in self._entries:

            if 2 > c.nEntries(): continue
            mn , mx = c.min() , c.max()
            dd = max ( abs ( mx ) , abs ( mn ) )
            if ( dd <= ddmax ) or ( not tune ) or ( ddmax < 0 ) or ( cnt is None ) : 
                ddmax = dd
                tune  = t
                cnt   = c 
    
        return tune, ddmax, cnt   
    
    def finalize ( self ):

        t , d , c  = self.the_tune ()
        if c : 
            m = float ( c.mean () )
            r = float ( c.rms  () )
        else :
            m = -1
            r = -1
            
        self.Print ( 'The most probable ANNPID tune is: %s/[%.3g;(%.3g +- %.3f)]' % ( t , d , m , r ) , SUCCESS , 10 )
        self.Print ( 'Statistic: %s ' % c                                     , SUCCESS , 10 )
                        
        del self._probnn
        del self._entries
        
        return Algo.finalize(self)

    ## the main 'analysis' method
    def analyse(self):  ## IMPORTANT!
        """The main 'analysis' method
        """
        ## get particles from the input locations
        particles = self.select ( 'ALL' , ALL )
        if not particles: return self.Warning ( "No particles are found", SUCCESS )

        ## loop over all particles and fill the counters
        nb = 0 
        for p in particles:

            basic = p.children ( ISBASIC & HASTRACK ) 

            for b in  basic :
                nb += 1 
                tv  = self._probnn ( b )  ##  the test value
                for c , f , t in self._entries:
                    c += f ( b ) - tv    ## differences

        self.setFilterPassed ( 0 < nb )
        
        return SUCCESS

# =============================================================================
## Create prescaled selection with CheckANNPID algorithm
#  @code
#  from StandardParticles import StdLooseMuons as muons
#  sel , alg_typ = check_annpid ( 'MUON' , muons )
#  @endcode
#  - The selection can be inserted into the overall dtaa flow:
#  @code
#  davinci.UserAlgorithm.append ( sel )
#  @endcode
#  - The algorithm is instantaited later:
#  @code
#  alg = alg_typ ( sel )
#  @endcode
def check_annpid ( name , input , prescale=0.001 ):
    """Create prescaled selection with ChckANNPID algorithm

    >>> from StandardParticles import StdLooseMuons as muons
    >>> sel , alg_typ = check_annpid ( 'MUON' , muons )

    The selection can be inserted into the overall dtaa flow:
    >>> davinci.UserAlgorithm.append ( sel )

    The algorithm is instantaited later:
    >>> alg = alg_typ ( sel )
    """

    assert name.upper() in particles, \
           "check_annpid: invalid particle name  %s"   % name

    from PhysConf.Selections import PrescaleEventSelection as Prescale

    ps = Prescale(prescale, 'CheckANNPID_%s' % name)

    inputs = [ ps ]
    if isinstance ( input , ( list , tuple ) ) :
        for i in input:
            inputs.append(i)
    else:
        inputs.append(input)

    from Bender.Selections import BenderSelection
    return BenderSelection('CheckANNPID_%s' % name, inputs), CheckANNPID


# =============================================================================
## configure 
def config_annpid ( config , colors=False):
    """
    """
    config.teslocation = config.teslocation.strip()
    # the basic configuration
    from BenderTools.DstExplorer import configure
    configure ( config , colors )

    root = config.RootInTES
    line = config.teslocation

    lu = line.upper() if line else config.Particle.upper() 

    if   lu in ( 'K+'  , 'K-'  , 'K'  ) : lu = 'KAON'
    elif lu in ( 'pi+' , 'pi-' , 'pi' ) : lu = 'PION'
    elif lu in ( 'mu+' , 'mu-' , 'mu' ) : lu = 'MUON'
    elif lu in ( 'e+'  , 'e-'  , 'e'  ) : lu = 'ELECTRON'
    elif lu in ( 'p+'  , 'p~-' , 'p'  ) : lu = 'PROTON'
    
    if   lu == 'KAON'     : line = 'StdAllNoPIDsKaons'
    elif lu == 'PION'     : line = 'StdAllNoPIDsPions'
    elif lu == 'MUON'     : line = 'StdAllNoPIDsMuons'
    elif lu == 'ELECTRON' : line = 'StdAllNoPIDsElectrons'
    elif lu == 'POSITRON' : line = 'StdAllNoPIDsElectrons'
    elif lu == 'PROTON'   : line = 'StdAllNoPIDsProtons'
    
    import StandardParticles
    il   = line 
    if hasattr ( StandardParticles , line ) :
        line = getattr ( StandardParticles , line )
        logger.info ('Input is defined as %s from StandardParticles' % il )
        from PhysConf.Selections import PrintSelection,  RebuildSelection
        line = RebuildSelection ( line )
        line = PrintSelection   ( line )        
    else :
        from PhysConf.Selections import AutomaticData
        line = AutomaticData ( line )
        logger.info ('Input is defined as %s via  AutomaticData'     % il ) 

    from Configurables import DaVinci 
    dv =  DaVinci()
    
    from Bender.Selections import BenderSelection

    nn = ''
    nl = config.Particle.lower()
    if   'kaon'     in nl : nn = 'KAON'
    elif 'pion'     in nl : nn = 'PION'
    elif 'muon'     in nl : nn = 'MUON'
    elif 'electron' in nl : nn = 'ELECTRON'
    elif 'positron' in nl : nn = 'ELECTRON'
    elif 'proton'   in nl : nn = 'PROTON'
    elif 'pi'       in nl : nn = 'PION'
    elif 'mu'       in nl : nn = 'MUON'
    elif 'k'        in nl : nn = 'KAON'
    elif 'e'        in nl : nn = 'ELECTRON'
    elif 'p'        in nl : nn = 'PROTON'
    else : nn = nl.replace('/','').replace(':','').upper() 

    particle = config.Particle.lower() 

    bsel = BenderSelection ( 'CheckANNPID_%s' % nn , line )

    dv.UserAlgorithms.append ( bsel ) 
                                       
    from BenderTools.Utils import silence, totalSilence

    
    if config.Quiet or 3 <  config.OutputLevel : silence      ()
    if config.Quiet or 5 <= config.OutputLevel : totalSilence ()
        
    from Bender.Main import appMgr
    gaudi = appMgr()

    alg = CheckANNPID ( bsel )

    return alg


# =============================================================================
if '__main__' == __name__:

    logger.info(80 * '*')
    logger.info(__doc__)
    logger.info(' Author  : %s' % __author__)
    logger.info(' Version : %s' % __version__)
    logger.info(' Date    : %s' % __date__)
    logger.info(' Symbols : %s' % list(__all__))
    logger.info(80 * '*')

# ==============================================================================
# The END
# ==============================================================================
