#!/usr/bin/env python 
# -*- coding: utf-8 -*-
# =============================================================================
# $Id$ 
# =============================================================================
## @file BenderTools/DstDump.py
#
#  Trivial Bender-based script to dump the content of (x,mu,s,r,...)DSTs
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  <b>``C++ ToolKit for Smart and Friendly Physics Analysis''</b>
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software''
#
# Usage:
#
# @code
#
#    dst_dump [options] file1 [ file2 [ file3 [ file4 ....'
#
#  @endcode 
#
#  @date   2011-10-24
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#
# =============================================================================
"""Trivial Bender-based script to dump the content of (x,mu,s,r,...)DSTs

oooooooooo.                              .o8                     
`888'   `Y8b                            \"888                     
 888     888  .ooooo.  ooo. .oo.    .oooo888   .ooooo.  oooo d8b 
 888oooo888' d88' `88b `888P\"Y88b  d88' `888  d88' `88b `888\"\"8P 
 888    `88b 888ooo888  888   888  888   888  888ooo888  888     
 888    .88P 888    .o  888   888  888   888  888    .o  888     
o888bood8P'  `Y8bod8P' o888o o888o `Y8bod88P\" `Y8bod8P' d888b    
                                                                 
This file is a part of BENDER project:

  ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project:
 
   ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement with the smear campaign of Dr.O.Callot et al.:

   ``No Vanya's lines are allowed in LHCb/Gaudi software''

Usage:

    dst_dump [options] file1 [ file2 [ file3 [ file4 ....'

"""
# =============================================================================
__author__  = 'Vanya BELYAEV Ivan.Belyaev@itep.ru'
__date__    = "2011-10-24"
__version__ = '$Revision$'
__all__     = ()  ## nothing to import 
__all__     =  ( 'dumpDst' , )
# =============================================================================
## logging
# =============================================================================
from Bender.Logger import getLogger
if '__main__' == __name__ : logger = getLogger ( 'BenderTools.DstDump' )
else                      : logger = getLogger ( __name__ )
# =============================================================================
## table format 
_fhdr_ = ' | %8s | %15s | %7s | %4s | %6s |'                 ## header  
_fmt1_ = ' | %8g | %7.1f+-%-6.1f | %-7.1f | %4d | %-6d |'  
_fmt2_ = ' | %8d | %7d%8s | %7s | %4s | %6s |'
# =============================================================================
def formatItem ( item ) : 
    if item.Min() != item.Max() or 0 != item.rms() or  0 != item.meanErr() :
        return  _fmt1_ % ( item.sum     () ,
                           item.mean    () ,
                           item.meanErr () ,
                           item.rms     () ,
                           long ( item.Min () ) ,
                           long ( item.Max () ) )
    else :
        return _fmt2_ % ( long ( item.sum  ().value() ) ,
                          long ( item.mean ().value() ) , '' , '' , '', '' ) 

# =============================================================================
## the function that prints the table 
def print_them ( objects , config , iEvent , excluded = {} ) :
    """The function that prints the table 
    """
    
    keys = objects.keys()
    keys.sort()

    length  = 25
    for key in keys : length = max ( length , len  ( key ) ) 
    length += 2-7

    _printMessage = []

    lline   = ' +' + ( length + 58 ) * '-' + '+'
    _printMessage += [ lline ] 

    message = _fhdr_ % ( 'Total ' , '     Mean     ' , '  rms  ' ,  'min' , ' max ' ) 
    message = " | %s %s" % ( 'Location'.ljust(length) , message )

    _printMessage += [ message ] 
    _printMessage += [ lline   ]

    for loc in keys :
        item     = objects[loc]

        l = loc.replace('/Event/','')

        if config.Zeroes :
            while item.nEntries() < _nevents_ : item += 0
                
        ## the actual formating 
        message = formatItem ( item )
        
        message = " | %s %s" % ( l.ljust(length) , message )
    
        _printMessage += [ message ] 
        
    _printMessage += [ lline ]
    if excluded :
        keys = excluded.keys()
        keys.sort() 
        maxlen = max ( [ len(k) for k in keys ] )
        fmt    = ' | %%%ds | %%-10d | ' % ( maxlen + 2 )
        fmt1   = ' | %%%ds | %%-10s |' % ( maxlen + 2 )
        
        _printMessage += [ "Excluded %s locations: " % len( excluded ) ]
        _printMessage += [ ' +' (maxlen+4)*'-' + '+' + 12*'-' +  '+ ' ]
        _printMessage += [ fmt1 % ( 'Location' , '#' ) ]
        for k in keys : _printMessage += [ fmt % ( k , excluded[k] ) ]            
        _printMessage += [ ' +' (maxlen+4)*'-' + '+' + 12*'-' +  '+ ' ]
    _printMessage += [ "   Analysed %d events" % iEvent  ] 


    logger.info ( 100*'*')
    print '\n\n\n'
    ofile  = open ( config.SummaryFile , 'w' )     
    for line in _printMessage :
        print           line   
        print >> ofile, line 
    ofile.close()
    print '\n\n\n'
    logger.info ( 100*'*')


# =============================================================================
## dump it ! 
#  @date   2011-10-24
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
def dumpDst ( config ) :
    ##
    from Bender.Logger import setLogging
    
    logger.info ( 100*'*') 
    if config.Quiet : 

        logger.info(' Trivial Bender-based script to explore the content of (x,mu,s,r,...)DSTs ' ) 
        import logging
        logging.disable ( logging.INFO  )
        config.OutputLevel = 5
        setLogging ( config.OutputLevel ) 
        
    else :
        
        setLogging ( config.OutputLevel )
        
        logger.info ( 100*'*')
        logger.info ( __doc__ ) 
        logger.info ( 100*'*')
        logger.info ( ' Author  : %s ' % __author__   ) 
        logger.info ( ' Version : %s ' % __version__  ) 
        logger.info ( ' Date    : %s ' % __date__     )
        logger.info ( 100*'*')

    from BenderTools.DstExplorer import configure 
    configure ( config , colors = config.Color ) 

    from BenderTools.Utils import totalSilence
    totalSilence( dod = config.DataOnDemand )
    
    from Bender.Main import appMgr, run, cpp
    if config.Simulation : import Bender.MainMC

    if 1000 < config.nEvents :
        logger.warning('Large number of events is required to process %s ' % config.nEvents )

    #
    ## instantiate the application manager
    #
    gaudi=appMgr ()

    import LHCbMath.Types 
    #
    ## copy the lines from Juan's script check_dst_contents.py
    #
    evtSvc = gaudi.evtSvc()
    SE     = cpp.StatEntity
    
    _nevents_ = 0 
    def addEntry ( dct , key , val ) :
        if not dct.has_key(key) : dct[key] = SE()
        se  = dct[key]
        se += float(val)
    
    dodSvc = gaudi.service('DataOnDemandSvc')
    
    nSelEvents = {}
    nObjects   = {}

    root = config.RootInTES 
    if not root :
        root = '/Event'
        logger.warning('Use "%s" as root, could be non-efficient' % root )

    ## exclude certain locations
    exclusions = [] 
    import  re 
    for item in config.Exclude : exclusions.append ( re.compile ( item ) ) 
    from collections import   defaultdict
    excluded = defaultdict(int)
    
    from Ostap.progress_bar import progress_bar
    from itertools          import count 
    for iEvent in  progress_bar (
        xrange ( config.nEvents ) if 0 < config.nEvents else count () ,
        silent = not config.Color ) :
        #
        sc   = run(1)
        _nevents_ += 1 
        if sc.isFailure()       : break
        #
        if not evtSvc['/Event'] : break
        ##
        ## iEvent += 1
        #
        nodes = evtSvc.nodes ( node      = root ,
                               forceload = True )
        if not nodes :
            logger.warning ( "No nodes are selected for Root:'%s'" % root  )

        nodes = set ( nodes ) 
        links = set ()
        dods  = set ()
    
        #
        ## explore the regular nodes
        #
        for loc in nodes :

            skip = False 
            for item in exclusions:
                if not item.match ( loc ) : continue 
                skip            = True
                excluded [loc] +=  1 
                break
            if skip : continue 
            
            loc0 = loc 
            data = evtSvc[loc]
            loc  = loc[:7] + ' ' + loc[7:] 
            if not data :
                addEntry ( nSelEvents , loc , 0 )
                addEntry ( nObjects   , loc , 0 )
            elif type( data ) == cpp.DataObject  : continue 
            else :
                #
                if   hasattr ( data , 'size'   ) : addEntry ( nObjects , loc , data.size()  )
                elif hasattr ( data , '__len__') : addEntry ( nObjects , loc , len ( data ) )
                else                             : addEntry ( nObjects , loc , 1            )
                #
                ## collect the links (if needed) 
                if config.FollowLinks: 
                    lnks = data.links()
                    for l in lnks : links.add ( l )
                    if '/pRec/DecReport' in loc0 : 
                        links.add ( loc0.replace ( '/pRec/'  , '/Rec/'  ) )
        #
        ## follow the links? Useful for packed (u)DST
        #
        if config.FollowLinks: 
            links = links - nodes 
            for loc in links:
                
                if config.RootInTES and not config.RootInTES in ( '/Event' , '/Event/' ) :  
                    if not config.RootInTES in loc : continue 
                    
                skip = False 
                for item in exclusions:
                    if not item.match ( loc ) : continue 
                    skip            = True
                    excluded [loc] +=  1 
                    break
                if skip : continue 

                data = evtSvc[loc]
                loc  = loc[:7] + '*' + loc[7:] 
                if          data is None             : continue 
                elif type ( data ) == cpp.DataObject : continue 
                elif not data :
                    addEntry ( nSelEvents , loc , 0 )
                    addEntry ( nObjects   , loc , 0 )
                elif hasattr ( data , 'size'   ) : addEntry ( nObjects , loc , data.size()  )
                elif hasattr ( data , '__len__') : addEntry ( nObjects , loc , len ( data ) )
                else                             : addEntry ( nObjects , loc , 1            )
                    
        #
        ## explore locations known for DOD
        #
        if config.DataOnDemand :
        
            for k in dodSvc.AlgMap .keys () : dods.add ( k ) 
            for k in dodSvc.NodeMap.keys () :
                obj = dodSvc.NodeMap[k]
                if 'DataObject' == obj : continue 
                dods.add ( k )
            
            dods = dods - nodes 
            dods = dods - links
        
            for loc in dods :

                if config.RootInTES :
                    if not config.RootInTES in loc : continue 
                
                skip = False 
                for item in exclusions:
                    if not item.match ( loc ) : continue 
                    skip            = True
                    excluded [loc] +=  1 
                    break
                if skip : continue 
                
                if not config.Simulation :
                    if 'MC'   in loc : continue 
                    if 'Prev' in loc : continue 
                    if 'Next' in loc : continue 
                    
                data = evtSvc[loc]
                loc = loc[:7] + '+' + loc[7:] 
                if not data :
                    addEntry ( nSelEvents , loc , 0 )
                    addEntry ( nObjects   , loc , 0 )
                elif type( data ) == cpp.DataObject : continue 
                else :
                #
                    if   hasattr ( data , 'size'   ) : addEntry ( nObjects , loc , data.size()  )
                    elif hasattr ( data , '__len__') : addEntry ( nObjects , loc , len ( data ) )
                    else                             : addEntry ( nObjects , loc , 1            )


        if 1 < config.DumpFrequency and 0 < iEvent and 0 == iEvent % config.DumpFrequency :
            print_them (  nObjects , config , iEvent + 1  , excluded )

    ## print the final table 
    print_them (  nObjects , config , iEvent , excluded )

# =============================================================================
if '__main__' == __name__ :
    
    logger.info ( 100*'*')
    logger.info ( __doc__ ) 
    logger.info ( 100*'*')
    logger.info ( ' Author  : %s ' % __author__   ) 
    logger.info ( ' Version : %s ' % __version__  ) 
    logger.info ( ' Date    : %s ' % __date__     )
    logger.info ( 100*'*')
    
    dumpDst ()

    logger.info ( 100*'*')
    
# =============================================================================
# The END 
# =============================================================================

   
