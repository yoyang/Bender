#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file BenderTools/Fill.py
#
# Decorate the algorithm with proper methods to fill n-tuples 
#  
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  <b>``C++ ToolKit for Smart and Friendly Physics Analysis''</b>
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software''
#
# @author Vanya BELYAEV Ivan.Belyaev@cern.ch
# @date   2011-06-21
#
#                   $Revision$
# Last modification $Date$
#                by $Author$
# =============================================================================
"""Decorate the algorithm with proper methods to fill n-tuples

oooooooooo.                              .o8                     
`888'   `Y8b                            \"888                     
 888     888  .ooooo.  ooo. .oo.    .oooo888   .ooooo.  oooo d8b 
 888oooo888' d88' `88b `888P\"Y88b  d88' `888  d88' `88b `888\"\"8P 
 888    `88b 888ooo888  888   888  888   888  888ooo888  888     
 888    .88P 888    .o  888   888  888   888  888    .o  888     
o888bood8P'  `Y8bod8P' o888o o888o `Y8bod88P\" `Y8bod8P' d888b    
                                                                 
This file is a part of BENDER project:

  ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project:
 
   ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement with the smear campaign of Dr.O.Callot et al.:

   ``No Vanya's lines are allowed in LHCb/Gaudi software''

"""
# =============================================================================
__version__ = "$Revision$"
__author__  = "Vanya BELYAEV Ivan.Belyaev@cern.ch"
__date__    = "2011-06-07"
# =============================================================================
__all__= (
    'treatPions'    , ## add information about pions 
    'treatKaons'    , ## add information about kaons 
    'treatProtons'  , ## add information about protons
    'treatMuons'    , ## add information about muons 
    'treatPhotons'  , ## add information about photons
    'treatDiGammas' , ## add information about di-photons (pi0, eta,...)
    'treatTracks'   , ## add information about the tracks
    'treatKine'     , ## add detailed information for particle ,
    'fillMasses'    , ## masses of sub-combinations
    'addRecSummary' , ## add rec-summary information
    'addGecInfo'    , ## add some GEC-info
    'ANNPIDTUNES'   , ## known ANNPID tunes 
    )
# ==============================================================================
from   LoKiCore.basic                import cpp,LoKi 
from   LoKiPhys.decorators           import *
from   GaudiKernel.SystemOfUnits     import GeV, MeV, mm 
# =============================================================================
# logging 
# =============================================================================
from Bender.Logger import getLogger 
if '__main__' == __name__ : logger = getLogger ( 'BenderTools.Fill' )
else                      : logger = getLogger ( __name__ )
# ==============================================================================
SUCCESS                 = cpp.StatusCode( cpp.StatusCode.SUCCESS , True )
FAILURE                 = cpp.StatusCode( cpp.StatusCode.FAILURE , True )
v_doubles               = cpp.std.vector('double')
# ==============================================================================
## constants: jets? 
#max_muons               =  30 ## maximal number of     muons in the decay tree 
#max_gamma               =  60 ## maximal number of    gammas in the decay tree 
#max_digamma             =  30 ## maximal number of di-gammas in the decay tree
#max_tracks              = 250 ## maximal number of    tracks in the decay tree
#max_protons             = 120 ## maximal number of   protons in the decay tree 
#max_kaons               = 120 ## maximal number of     kaons in the decay tree 
#max_pions               = 250 ## maximal number of     pions in the decay tree 
# ==============================================================================
#max_muons               =  10 ## maximal number of     muons in the decay tree 
#max_gamma               =  10 ## maximal number of    gammas in the decay tree 
#max_digamma             =  10 ## maximal number of di-gammas in the decay tree
#max_tracks              =  10 ## maximal number of    tracks in the decay tree
#max_protons             =  10 ## maximal number of   protons in the decay tree 
#max_kaons               =  10 ## maximal number of     kaons in the decay tree 
#max_pions               =  10 ## maximal number of     pions in the decay tree 
# ==============================================================================
max_muons               =  10 ## maximal number of     muons in the decay tree 
max_gamma               =  10 ## maximal number of    gammas in the decay tree 
max_digamma             =  10 ## maximal number of di-gammas in the decay tree
max_tracks              =  20 ## maximal number of    tracks in the decay tree
max_protons             =  20 ## maximal number of   protons in the decay tree 
max_kaons               =  20 ## maximal number of     kaons in the decay tree 
max_pions               =  20 ## maximal number of     pions in the decay tree 
# ==============================================================================
## initialize the internal machinery
def fill_initialize ( self ) :
    """Initialize the internal machinery 
    """
    logger.debug('Initialize "fill" machinery') 
    self._pions           = LoKi.Child.Selector ( 'pi+'   == ABSID )
    self._kaons           = LoKi.Child.Selector ( 'K+'    == ABSID )
    self._protons         = LoKi.Child.Selector ( 'p+'    == ABSID )
    self._muons           = LoKi.Child.Selector ( 'mu+'   == ABSID )
    self._gamma           = LoKi.Child.Selector ( 'gamma' == ID    )
    self._digamma         = LoKi.Child.Selector ( DECTREE ( ' ( pi0 | eta ) -> <gamma> <gamma>' ) ) 
    ##self._digamma         = LoKi.Child.Selector ( '( pi0 | eta ) -> <gamma> <gamma>' ) 
    self._pi0             = LoKi.Child.Selector ( 'pi0'   == ID    )
    self._tracks          = LoKi.Child.Selector (       HASTRACK   )
    self._basic           = LoKi.Child.Selector (        ISBASIC   )
    #
    self._ctau            = BPVLTIME ( self ,    ) * c_light
    self._ctau_9          = BPVLTIME ( self ,  9 ) * c_light
    self._ctau_25         = BPVLTIME ( self , 25 ) * c_light
    self._lv01            = LV01  
    self._vchi2           = CHI2VX 
    self._vchi2ndf        = CHI2VXNDF 
    self._dtfchi2         = DTF_CHI2NDOF ( self , True )
    self._ipchi2          = BPVIPCHI2    ( self )
    self._dls             = LoKi.Particles.DecayLengthSignificanceDV ( self )
    self._tz              = BPVTZ        ( self )
    
    ## can be used to evaluate IP ?
    _BT = ISBASIC & HASTRACK 
    self._ok_for_ip       = ( ISBASIC & HASTRACK ) | ( ~ISBASIC & INTREE ( ISBASIC & HASTRACK ) ) 
    
    self._min_dll_K       = MINTREE ( 'K+'  == ABSID   , PIDK  - PIDpi ) 
    self._min_dll_Pi      = MINTREE ( 'pi+' == ABSID   , PIDpi - PIDK  ) 
    self._min_dll_PK      = MINTREE ( 'p+'  == ABSID   , PIDp  - PIDK  ) 
    self._min_dll_Ppi     = MINTREE ( 'p+'  == ABSID   , PIDp  - PIDpi ) 
    self._min_dll_Mu      = MINTREE ( 'mu+' == ABSID   , PIDmu - PIDpi )
    #
    self._min_annpid_K    = MINTREE ( 'K+'  == ABSID     , PROBNNk      ) 
    self._min_annpid_Pi   = MINTREE ( 'pi+' == ABSID     , PROBNNpi     ) 
    self._min_annpid_Mu   = MINTREE ( 'mu+' == ABSID     , PROBNNmu     ) 
    self._min_annpid_E    = MINTREE ( 'e+'  == ABSID     , PROBNNe      )
    self._min_annpid_P    = MINTREE ( 'p+'  == ABSID     , PROBNNp      )
    
    from LoKiCore.functions import switch 
    self._has_rich        = switch  ( HASRICH , 1 , 0 ) 
    #
    self._min_Pt          = MINTREE ( ISBASIC & HASTRACK , PT  ) / GeV 
    self._min_Eta         = MINTREE ( ISBASIC & HASTRACK , ETA ) 
    self._max_Eta         = MAXTREE ( ISBASIC & HASTRACK , ETA )
    #
    self._min_Pt_pi       = MINTREE ( 'pi+' == ABSID , PT  ) / GeV 
    self._min_P_pi        = MINTREE ( 'pi+' == ABSID , P   ) / GeV 
    self._min_Eta_pi      = MINTREE ( 'pi+' == ABSID , ETA ) 
    self._max_Eta_pi      = MAXTREE ( 'pi+' == ABSID , ETA )
    #
    self._min_Pt_k        = MINTREE ( 'K+'  == ABSID , PT  ) / GeV 
    self._min_P_k         = MINTREE ( 'K+'  == ABSID , P   ) / GeV 
    self._min_Eta_k       = MINTREE ( 'K+'  == ABSID , ETA ) 
    self._max_Eta_k       = MAXTREE ( 'K+'  == ABSID , ETA )
    #
    self._min_Pt_p        = MINTREE ( 'p+'  == ABSID , PT  ) / GeV 
    self._min_P_p         = MINTREE ( 'p+'  == ABSID , P   ) / GeV 
    self._min_Eta_p       = MINTREE ( 'p+'  == ABSID , ETA ) 
    self._max_Eta_p       = MAXTREE ( 'p+'  == ABSID , ETA )
    #
    self._min_Pt_mu       = MINTREE ( 'mu+' == ABSID , PT  ) / GeV 
    self._min_P_mu        = MINTREE ( 'mu+' == ABSID , P   ) / GeV 
    self._min_Eta_mu      = MINTREE ( 'mu+' == ABSID , ETA ) 
    self._max_Eta_mu      = MAXTREE ( 'mu+' == ABSID , ETA )

    self._min_CL_gamma    = MINTREE ( 'gamma' == ID , CL ) 
    self._min_Et_gamma    = MINTREE ( 'gamma' == ID , PT ) / GeV 
    self._min_E_gamma     = MINTREE ( 'gamma' == ID ,  E ) / GeV 
    #
    self._maxTrChi2       = MAXTREE ( ISBASIC & HASTRACK , TRCHI2DOF    )
    self._maxTrGhost      = MAXTREE ( ISBASIC & HASTRACK , TRGHOSTPROB  )
    self._minTrKL         = MINTREE ( ISBASIC & HASTRACK , CLONEDIST    ) 
    self._minTrIPchi2     = MINTREE ( ISBASIC & HASTRACK , BPVIPCHI2 ( self ) )
    self._max_anngh_track = MAXTREE ( ISBASIC & HASTRACK , PROBNNghost  )
    #
    ##
    self._EtC             = PINFO   ( 55001 , -100 * GeV ) 
    self._PtC             = PINFO   ( 55002 , -100 * GeV )  
    self._maxEtC          = MAXTREE ( 'mu+' == ABSID , self._EtC ) 
    self._maxPtC          = MAXTREE ( 'mu+' == ABSID , self._PtC )
    ## 
    self._EcalE           = PPINFO  ( LHCb.ProtoParticle.CaloEcalE , -100 * GeV ) 
    self._HcalE           = PPINFO  ( LHCb.ProtoParticle.CaloHcalE , -100 * GeV )  
    self._maxEcalE        = MAXTREE ( 'mu+' == ABSID , self._EcalE )
    self._maxHcalE        = MAXTREE ( 'mu+' == ABSID , self._HcalE )
    
    self._flagmu          = switch ( ISBASIC & HASTRACK ,                                   
                                     1    * switch ( HASMUON     , 1 , 0 ) +
                                     10   * switch ( ISMUONLOOSE , 1 , 0 ) +
                                     100  * switch ( ISMUON      , 1 , 0 ) +
                                     1000 * switch ( ISMUONTIGHT , 1 , 0 ) , -1 )
    self._nsharedmu       = NSHAREDMU 
    #
    ## gamma-ID
    #
    # ProtoParticle.h: 
    # PhotonID = 380, // Combined PDF for photonID - OBSOLETE DLL-based neutralID (neutral)
    # IsPhoton = 381, // Gamma/Pi0 separation variable (neutral)
    # IsNotE = 382,   // MLP-based neutralID - anti-electron ID (neutral)
    # IsNotH = 383,   // MLP-based neutralID - anti-hadron ID   (neutral)
    self._IsNotE          = PPINFO ( LHCb.ProtoParticle.IsNotE          , -1.e+6 ) 
    self._IsNotH          = PPINFO ( LHCb.ProtoParticle.IsNotH          , -1.e+6 ) 
    self._IsPhoton        = PPINFO ( LHCb.ProtoParticle.IsPhoton        , -1.e+6 ) 
    self._PhotonID        = PPINFO ( LHCb.ProtoParticle.PhotonID        , -1.e+6 )
    self._CaloHypoSpdM    = PPINFO ( LHCb.ProtoParticle.CaloNeutralSpd  , -1     )
    # 
    self._min_IsNotE_gamma   = MINTREE ( 'gamma' == ID , self._IsNotE   ) 
    self._min_IsNotH_gamma   = MINTREE ( 'gamma' == ID , self._IsNotH   ) 
    self._min_IsPhoton_gamma = MINTREE ( 'gamma' == ID , self._IsPhoton ) 
    self._min_PhotonID_gamma = MINTREE ( 'gamma' == ID , self._PhotonID ) 
    #
    self._cellID          = LoKi.Photons.cellID  ## get the seed for the given photon
    #
    
    
    self._delta_m2        = LoKi.Kinematics.deltaM2
    
    self.pion_mass = LoKi.Particles.massFromName ('pi+')
    assert 130 * MeV < self.pion_mass < 145 * MeV , 'Invalid mass of pion!'
    
    
    self._delta_alpha     = LoKi.PhysKinematics.deltaAlpha
    self._masses          = {}
    self._overlap         = LHCb.HashIDs.overlap 
    #

    ## jet ?
    self._jet = ( ID == 'CLUSjet' ) | ( ID == 'CELLjet' ) 
    
    self.fill_initialized = True

    self._pions_init_     = False
    self._kaons_init_     = False
    self._muons_init_     = False
    self._protons_init_   = False
    self._photons_init_   = False
    self._digammas_init_  = False
    self._tracks_init_    = False
    self._kine_init_      = False
    self._masses_init_    = False
    self._recinfo_init_   = False
    self._gecinfo_init_   = False  

    return SUCCESS 

    
# ==============================================================================
## finalize 
def fill_finalize   ( self ) :
    """ Finalize the internal machinery 
    """
    if not self.fill_initialized : return SUCCESS
    if     self.fill_finalized   : return SUCCESS
    
    del self._pions           # = None 
    del self._kaons           # = None 
    del self._protons         # = None 
    del self._muons           # = None 
    del self._gamma           # = None 
    del self._digamma         # = None 
    del self._pi0             # = None 
    del self._tracks          # = None 
    del self._basic           # = None 
    del self._ok_for_ip       # = None 
    #
    del self._ctau            # = None 
    del self._ctau_9          # = None 
    del self._ctau_25         # = None 
    del self._lv01            # = None 
    del self._vchi2           # = None 
    del self._vchi2ndf        # = None 
    del self._dtfchi2         # = None 
    del self._ipchi2          # = None 
    del self._dls             # = None 
    #
    del self._min_dll_K       # = None 
    del self._min_dll_Pi      # = None 
    del self._min_dll_PK      # = None 
    del self._min_dll_Ppi     # = None 
    del self._min_dll_Mu      # = None
    #
    del self._min_annpid_K    # = None 
    del self._min_annpid_Pi   # = None 
    del self._min_annpid_Mu   # = None 
    del self._min_annpid_E    # = None 
    del self._min_annpid_P    # = None 

    del self._has_rich        # = None
    
    del self._min_Pt          # = None 
    del self._min_Eta         # = None 
    del self._max_Eta         # = None

    del self._min_Pt_pi       # = None 
    del self._min_P_pi        # = None 
    del self._min_Eta_pi      # = None 
    del self._max_Eta_pi      # = None

    del self._min_Pt_k        # = None 
    del self._min_P_k         # = None 
    del self._min_Eta_k       # = None 
    del self._max_Eta_k       # = None

    del self._min_Pt_p        # = None 
    del self._min_P_p         # = None 
    del self._min_Eta_p       # = None 
    del self._max_Eta_p       # = None
    #
    del self._min_Pt_mu       # = None 
    del self._min_P_mu        # = None 
    del self._min_Eta_mu      # = None 
    del self._max_Eta_mu      # = None

    del self._min_E_gamma     # = None 
    del self._min_Et_gamma    # = None 
    del self._min_CL_gamma    # = None 
    #
    del self._maxTrChi2       # = None 
    del self._maxTrGhost      # = None 
    del self._minTrKL         # = None 
    del self._minTrIPchi2     # = None 
    del self._max_anngh_track # = None 
    #
    del self._EtC             # = None 
    del self._PtC             # = None 
    del self._maxEtC          # = None 
    del self._maxPtC          # = None 
    ## 
    del self._EcalE           # = None
    del self._HcalE           # = None 
    del self._maxEcalE        # = None 
    del self._maxHcalE        # = None 
    #
    del self._flagmu          # = None 
    del self._nsharedmu       # = None 
    del self._IsNotE          
    del self._IsNotH          
    del self._IsPhoton        
    del self._PhotonID        
    #
    del self._min_IsNotE_gamma   
    del self._min_IsNotH_gamma   
    del self._min_IsPhoton_gamma 
    del self._min_PhotonID_gamma 
    #
    del self._cellID
    # 
    del self._delta_m2        # = None
    del self._delta_alpha     # = None
    del self._masses          # = None
    del self._overlap         # = None 
    #
    self.fill_finalized = True
    #
    return SUCCESS 

# =============================================================================
def _doc_ ( message ) :
    return message.replace('\n','\n# ')

# =============================================================================
## the list of known ANNPID tunes
class ANNPIDTUNES(object) :
    __slots__ = ( 'known_tunes' )
    from Configurables import TupleToolANNPID
    known_tunes = frozenset( TupleToolANNPID().ANNPIDTunes )

# =============================================================================
## get ANNPID functors for various particle types  and tunes
#  @code
#  tup   = ...
#  kaons = ...
#  tunes = self.annPidTunes ( 'Kaon' , '' , tunes = ( 'MC15TuneDNNV1', 'MC15TuneV1' ) )
#  for tune, func  in tunes :
#        sc = tup.fArrayP ( 'ann_pid_K_'  + tune , func.KAON   ,
#                           'ann_pid_PI_' + tune , func.PION   ,
#                           'ann_pid_P_'  + tune , func.PROTON ,
#                            kaons , 'n_k' , 20 )
#  @endcode
#  - <code>func.MUON</code>,
#  - <code>func.ELECTRON</code>
#  - <code>func.GHOST</code> are also available 
#  @see LoKi::Particles::ANNPID 
def annPidTunes ( self , particle , suffix = '' , tunes =  () , check = True ) :
    """Get ANNPID functors for various particle types  and tunes
    
    tup   = ...   ##  the N-tuple 
    kaons = ...   ##  list of kaons 
    tunes = self.annPidTunes ( 'Kaon' , tunes = ( 'MC15TuneDNNV1', 'MC15TuneV1' ) ) 
    for tune, func  in tunes :
    ...   sc = tup.fArrayP ( 'ann_pid_K_'  + tune , func.KAON   , ## NB! 
    ...                      'ann_pid_PI_' + tune , func.PION   , ## NB! 
    ...                      'ann_pid_P_'  + tune , func.PROTON , ## NB!
    ...                      kaons , 'n_k' , 20 )
    Also available:
    - func.MUON
    - func.ELECTRON
    - func.GHOST
    """
    if not tunes : return ()

    the_key  = '_ann_pid_tunes_%s_%s' % ( particle , suffix )
    try :
        return getattr ( self , the_key )
    except AttributeError :
        pass
    

    if isinstance ( tunes , str ) :
        if  'ALL' == tunes.upper() :
            tunes = ANNPIDTUNES.known_tunes 
        else : 
            tunes = tunes,
            
    if tunes and check and not ( tunes is ANNPIDTUNES.known_tunes ) :  
        ## use only valid known tunes 
        tunes = ANNPIDTUNES.known_tunes.intersection ( tunes ) 

    ## @class  ANNPID
    #  helper class to keep all ANNPID functors together 
    class ANNPIDTUNE(object) :
        """Helper class to keep all  ANNPID functors together"""
        __slots__  = (  'PION' , 'KAON' , 'PROTON' , 'MUON' , 'ELECTRON' , 'GHOST' , 'tune')
        def __init__ ( self , tune )  :
            self.PION     = ANNPID ( 'PION'     , tune )
            self.KAON     = ANNPID ( 'KAON'     , tune )
            self.PROTON   = ANNPID ( 'PROTON'   , tune )
            self.MUON     = ANNPID ( 'MUON'     , tune )
            self.ELECTRON = ANNPID ( 'ELECTRON' , tune )
            self.GHOST    = ANNPID ( 'GHOST'    , tune )
            self.tune     = tune 
            
    tunes_ = []
    for tune in tunes :
        try : 
            pair = tune , ANNPIDTUNE( tune ) 
            tunes_.append ( pair ) 
        except NameError :
            logger.error ( 'annPidTunes: ANNPID functor is not yet available yet, skip it!')
            pass
        
    tunes_.sort() 
    tunes_ = tuple (  tunes_ )
    setattr (  self , the_key , tunes_ )
    return tunes_ 

# =============================================================================
## Add pion information into n-tuple
#  @code
#  tup = ...  ## n-tuple 
#  B   = ...  ## the particle or loop-object
#  self.treatPions  ( tup , B  , '_b'   )  ## use suffix to mark variables 
#  @endcode
# 
# Following variables are added into n-tuple 
# - <code>mindll_piK[+suffix]</code>
# minimal value of  DLL(pi-K) for all pions
# - <code>minann_pi[+suffix]</code>
# minimal value of  PROBNNpi  for all pions
# - <code>n_pion</code>  Number of pions in the decay
# minimal PT for all pions:
# - <code>minPt_pion[+suffix]</code>  Minimal PT  for all pions (in GeV/c)
# minimal P  for all kaons:
# - <code>minP_pion[+suffix]</code>   Minimal P   for all pions (in GeV/c)
# minimal pseudorapidity for all pions
# - <code>minEta_pion[+suffix]</code> Minimal ETA for all pions
# maximal pseudorapidity for all pions
# - <code>maxEta_pion[+suffix]</code> Maximal ETA for all pions
#
# Then for each pion in the decay:
# - <code>p_pion[+suffix]</code>       Pion momentum 
# - <code>pt_pion[+suffix]</code>      Pion transverse momentum 
# - <code>eta_pion[+suffix]</code>     Pion pseudorapidity 
# - <code>hasRich_pion[+suffix]</code> Has it RICH information?(==``Is pion in RICH acceptance?'')
# - <code>pid_pion[+suffix]</code>     PID information: DLL(pi-K) 
# - <code>ann_pion[+suffix]</code>     PID information: PROBNNpi  (probability to be a pion) 
# - <code>ann_pion_P[+suffix]</code>   PID information: PROBNNp   (probability to be a proton)
# - <code>ann_pion_K[+suffix]</code>   PID information: PROBNNp   (probability to be a kaon)
#
# For each pion and for each ANNPID tune :
# - <code>annpid_pion[+suffix]_[tune]</code>    PROBBNpi    for given tune
# - <code>annpid_pion_K[+suffix]_[tune]</code>  PROBBNK     for given tune
# - <code>annpid_pion_P[+suffix]_[tune]</code>  PROBBNp     for given tune
# - <code>annpid_pion_GH[+suffix]_[tune]</code> PROBBNghost for given tune
#
# @param tup   n-tuple
# @param p     the particle
# @param suffix (optional) suffix to be added to names of all variables
def treatPions ( self         ,
                 tup          ,
                 p            ,
                 suffix  = '' ,
                 tunes   = () ) :
    """Add  pion information into n-tuple
    ...
    tup = ... ## n-tuple 
    b   = ... ## the particle  (or looping object)
    self.treatPions ( tup , b , '_B' ) ## prefix is optional 
    ...
    Following variables are added into n-tuple:
    - mindll_piK[+suffix]
    minimal value of  DLL(pi-K) for all pions
    - minann_pi[+suffix]
    minimal value of  PROBNNpi  for all pions
    - n_pion  Number of pions in the decay

    minimal PT for all pions:
    - minPt_pion[+suffix]  Minimal PT  for all pions (in GeV/c)
    minimal P  for all kaons:
    - minP_pion[+suffix]   Minimal P   for all pions (in GeV/c)
    minimal pseudorapidity for all pions
    - minEta_pion[+suffix] Minimal ETA for all pions
    maximal pseudorapidity for all pions
    - maxEta_pion[+suffix] Maximal ETA for all pions
    
    
    Then for each pion in the decay:
    - p_pion[+suffix]       Pion momentum 
    - pt_pion[+suffix]      Pion transverse momentum 
    - eta_pion[+suffix]     Pion pseudorapidity 
    - hasRich_pion[+suffix] Has it RICH information?(==``Is pion in RICH acceptance?'')
    - pid_pion[+suffix]     PID information: DLL(pi-K) 
    - ann_pion[+suffix]     PID information: PROBNNpi  (probability to be a pion) 
    - ann_pion_P[+suffix]   PID information: PROBNNp   (probability to be a proton)
    - ann_pion_K[+suffix]   PID information: PROBNNp   (probability to be a kaon)
    
    For each pion and for each ANNPID tune :
    - annpid_pion[+suffix]_[tune]    PROBBNpi    for given tune
    - annpid_pion_K[+suffix]_[tune]  PROBBNK     for given tune
    - annpid_pion_P[+suffix]_[tune]  PROBBNp     for given tune
    - annpid_pion_GH[+suffix]_[tune] PROBBNghost for given tune

    """
    #
    ## initialize it
    #
    if not self.fill_initialized : fill_initialize ( self )
    ##
    if not self._pions_init_ :
        _fun_ = treatPions
        logger.info  ( "%s: %s" % ( _fun_.__name__ , _doc_ ( _fun_.__doc__ ) ) )
        self._pions_init_ = True

    if hasattr ( p , 'particle' ) : p = p.particle() 
    #
    ## get all pions from the decay tree
    #
    good  = LHCb.Particle.ConstVector()
    p.children ( self._pions , good ) 
    #
    _cnt  = self.counter ( '#pion' + suffix )
    _cnt += good.size()
    #
    sc = tup.column_float ( 'mindll_piK'  + suffix , self._min_dll_Pi    ( p ) ) 
    sc = tup.column_float ( 'minann_pi'   + suffix , self._min_annpid_Pi ( p ) )
    ##
    sc = tup.column_float ( 'minPt_pion'  + suffix , self._min_Pt_pi     ( p ) )
    sc = tup.column_float ( 'minP_pion'   + suffix , self._min_P_pi      ( p ) )
    sc = tup.column_float ( 'minEta_pion' + suffix , self._min_Eta_pi    ( p ) )
    sc = tup.column_float ( 'maxEta_pion' + suffix , self._max_Eta_pi    ( p ) )
    #
    ##
    if  tunes :
        tunes_  = self.annPidTunes ( 'PION' , suffix , tunes )
        for t,pid in tunes_ :
            tup.fArrayP ( 'annpid_pion'    + suffix + '_' + t , pid.PION   ,
                          'annpid_pion_K'  + suffix + '_' + t , pid.KAON   ,
                          'annpid_pion_P'  + suffix + '_' + t , pid.PROTON , 
                          'annpid_pion_GH' + suffix + '_' + t , pid.GHOST  , 
                          LHCb.Particle.Range ( good )                     ,
                          'n_pion'        + suffix           , max_pions   )
    ##
    sc = tup.fArrayP (
        'p_pion'       + suffix , P   / GeV      ,  
        'pt_pion'      + suffix , PT  / GeV      , 
        'eta_pion'     + suffix , ETA            , 
        'hasRich_pion' + suffix , self._has_rich ,  
        LHCb.Particle.Range ( good )             ,
        'n_pion'       + suffix , max_pions      )
    return tup.fArrayP (
        'pid_pion'     + suffix , PIDpi - PIDK   ,  
        'ann_pion'     + suffix , PROBNNpi       ,  
        'ann_pion_P'   + suffix , PROBNNp        ,  
        'ann_pion_K'   + suffix , PROBNNK        ,  
        LHCb.Particle.Range ( good )             , 
        'n_pion'       + suffix , max_pions      )

# ==============================================================================
## add kaon information into n-tuple
#  @code
#  tup = ...  ## n-tuple 
#  B   = ...  ## the particle or loop-object
#  self.treatKaons  ( tup , B  , '_b'   )  ## use suffix to mark variables 
#  @endcode
# 
# Following variables are added into n-tuple 
# - <code>mindll_K[+suffix]</code>
# minimal value of  DLL(K-pi) for all kaons
# - <code>minann_K[+suffix]</code>
# minimal value of  PROBNNk  for all kaons
# - <code>n_kaon</code>  Number of pions in the decay
# minimal PT for all kaons:
# - <code>minPt_kaon[+suffix]</code>  Minimal PT  for all kaons (in GeV/c)
# minimal P  for all kaons:
# - <code>minP_kaon[+suffix]</code>   Minimal P   for all kaons (in GeV/c)
# minimal pseudorapidity for all kaons:
# - <code>minEta_kaon[+suffix]</code> Minimal ETA for all kaons
# maximal pseudorapidity for all kaons:
# - <code>maxEta_kaon[+suffix]</code> Maximal ETA for all kaons
#    
# Then for each pion in the decay:
# - <code>p_kaon[+suffix]</code>       Pion momentum 
# - <code>pt_kaon[+suffix]</code>      Pion transverse momentum 
# - <code>eta_kaon[+suffix]</code>     Pion pseudorapidity 
# - <code>hasRich_kaon[+suffix]</code> Has it RICH information?(==``Is kaon in RICH acceptance?'')
# - <code>pid_kaon[+suffix]</code>     PID information: DLL(pi-K) 
# - <code>ann_kaon[+suffix]</code>     PID information: PROBNNk  (probability to be a kaon) 
# - <code>ann_kaon_PI[+suffix]</code>  PID information: PROBNNpi (probability to be a pion)
# - <code>ann_kaon_P[+suffix]</code>   PID information: PROBNNp  (probability to be a proton)
#
# For each kaon and for each ANNPID tune :
# - <code>annpid_kaon[+suffix]_[tune]</code>    PROBBNK     for given tune
# - <code>annpid_kaon_PI[+suffix]_[tune]</code> PROBBNpi    for given tune
# - <code>annpid_kaon_P[+suffix]_[tune]</code>  PROBBNp     for given tune
# - <code>annpid_kaon_GH[+suffix]_[tune]</code> PROBBNghost for given tune
#
#  @param tup   n-tuple
#  @param p     the particle 
def treatKaons ( self         ,
                 tup          ,
                 p            ,
                 suffix  = '' ,
                 tunes   = () ) :
    
    """Add  Kaon information into n-tuple    
    ...
    tup = ... ## n-tuple 
    b   = ... ## the particle  (or looping object)
    self.treatKaons ( tup , b , '_B' ) ## prefix is optional 
    ...
    Following variables are added into n-tuple:
    - mindll_K[+suffix]
    minimal value of  DLL(K-pi) for all kaons
    - minann_K[+suffix]
    minimal value of  PROBNNk  for all kaons
    - n_kaon[+suffix]  Number of kaons in the decay
    
    minimal PT for all kaons:
    - minPt_kaon[+suffix]  Minimal PT  for all kaons (in GeV/c)
    minimal P  for all kaons:
    - minP_kaon[+suffix]   Minimal P   for all kaons (in GeV/c)
    minimal pseudorapidity for all kaons:
    - minEta_kaon[+suffix] Minimal ETA for all kaons
    maximal pseudorapidity for all kaons:
    - maxEta_kaon[+suffix] Maximal ETA for all kaons
    
    Then for each pion in the decay:
    - p_kaon[+suffix]       Kaon momentum 
    - pt_kaon[+suffix]      Kaon transverse momentum 
    - eta_kaon[+suffix]     KAon pseudorapidity 
    - hasRich_kaon[+suffix] Has it RICH information?(==``Is kaon in RICH acceptance?'')
    - pid_kaon[+suffix]     PID information: DLL(K-pi) 
    - ann_kaon[+suffix]     PID information: PROBNNk   (probability to be a kaon) 
    - ann_kaon_PI[+suffix]  PID information: PROBNNpi  (probability to be a pion)
    - ann_kaon_P[+suffix]   PID information: PROBNNp   (probability to be a proton)
    
    For each kaon and for each ANNPID tune :
    - annpid_kaon[+suffix]_[tune]    PROBBNK     for given tune
    - annpid_kaon_PI[+suffix]_[tune] PROBBNpi    for given tune
    - annpid_kaon_P[+suffix]_[tune]  PROBBNp     for given tune
    - annpid_kaon_GH[+suffix]_[tune] PROBBNghost for given tune
    
    """
    #
    ## initialize it
    #
    if not self.fill_initialized : fill_initialize ( self )
    ## 
    if not self._kaons_init_ :
        _fun_ = treatKaons
        logger.info  ( "%s: %s" % ( _fun_.__name__ , _fun_.__doc__ ) )
        self._kaons_init_ = True
    # 
    if hasattr ( p , 'particle' ) : p = p.particle() 
    #
    ## get all kaons from the decay tree
    #
    good  = LHCb.Particle.ConstVector()
    p.children ( self._kaons , good ) 
    #
    _cnt  = self.counter ( '#kaon' + suffix )
    _cnt += good.size()
    #
    sc = tup.column_float ( 'mindll_K' + suffix , self._min_dll_K     ( p ) ) 
    sc = tup.column_float ( 'minann_K' + suffix , self._min_annpid_K  ( p ) ) 
    #
    sc = tup.column_float ( 'minPt_kaon'  + suffix , self._min_Pt_k     ( p ) )
    sc = tup.column_float ( 'minP_kaon'   + suffix , self._min_P_k      ( p ) )
    sc = tup.column_float ( 'minEta_kaon' + suffix , self._min_Eta_k    ( p ) )
    sc = tup.column_float ( 'maxEta_kaon' + suffix , self._max_Eta_k    ( p ) )
    #
    if  tunes :
        tunes_  = self.annPidTunes ( 'KAON' , suffix , tunes )
        for t,pid in tunes_ :
            tup.fArrayP ( 'annpid_kaon'    + suffix + '_' + t , pid.KAON   ,
                          'annpid_kaon_PI' + suffix + '_' + t , pid.PION   ,
                          'annpid_kaon_P'  + suffix + '_' + t , pid.PROTON , 
                          'annpid_kaon_GH' + suffix + '_' + t , pid.GHOST  , 
                          LHCb.Particle.Range ( good )                     ,
                          'n_kaon'        + suffix            , max_kaons  )
    ##    
    tup.fArrayP (
        'p_kaon'       + suffix , P   / GeV      , 
        'pt_kaon'      + suffix , PT  / GeV      , 
        'eta_kaon'     + suffix , ETA            ,  
        'hasRich_kaon' + suffix , self._has_rich ,  
        LHCb.Particle.Range ( good )             ,
        'n_kaon'       + suffix , max_kaons      )
    return tup.fArrayP (
        'pid_kaon'     + suffix , PIDK - PIDpi   ,  
        'ann_kaon'     + suffix , PROBNNk        ,
        'ann_kaon_PI'  + suffix , PROBNNpi       ,  
        'ann_kaon_P'   + suffix , PROBNNp        ,  
        LHCb.Particle. Range ( good )            ,
        'n_kaon'       + suffix , max_kaons      )

# ==============================================================================
## add proton information into n-tuple
#  @code
#  tup = ...  ## n-tuple 
#  B   = ...  ## the particle or loop-object
#  self.treatProtons  ( tup , B  , '_b'   )  ## use suffix to mark variables 
#  @endcode
# 
# Following variables are added into n-tuple 
# - <code>mindll_pK[+suffix]</code>
# minimal value of  DLL(p-K)  for all protons
# - <code>mindll_ppi[+suffix]</code>
# minimal value of  DLL(p-pi) for all protons
# - <code>minann_P[+suffix]</code>
# minimal value of  PROBNNp   for all protons
# - <code>n_protons</code>  Number of protons in the decay
#    
# Then for each proton in the decay:
# - <code>p_proton[+suffix]</code>       Proton momentum 
# - <code>pt_proton[+suffix]</code>      Proton transverse momentum 
# - <code>eta_proton[+suffix]</code>     Proton pseudorapidity 
# - <code>hasRich_proton[+suffix]</code> Has it RICH information?(==``Is proton in RICH acceptance?'')
# - <code>pid_proton_pi[+suffix]</code>  PID information: DLL(p-pi) 
# - <code>pid_proton_K[+suffix]</code>   PID information: DLL(p-K) 
# - <code>ann_proton[+suffix]</code>     PID information: PROBNNp  (probability to be a proton) 
# - <code>ann_proton_PI[+suffix]</code>  PID information: PROBNNpi (probability to be a pion)
# - <code>ann_proton_K[+suffix]</code>   PID information: PROBNNp  (probability to be a kaon)
#
# For each proton and for each ANNPID tune :
# - <code>annpid_proton[+suffix]_[tune]</code>    PROBBNp     for given tune
# - <code>annpid_proton_PI[+suffix]_[tune]</code> PROBBNpi    for given tune
# - <code>annpid_proton_K[+suffix]_[tune]</code>  PROBBNK     for given tune
# - <code>annpid_proton_GH[+suffix]_[tune]</code>   PROBBNghost for given tune
#
#  @param tup   n-tuple
#  @param p     the particle 
def treatProtons ( self         ,
                   tup          ,
                   p            ,
                   suffix  = '' ,
                   tunes   = () ) :
    """Add proton information into n-tuple
    ...
    tup = ... ## n-tuple 
    b   = ... ## the particle  (or looping object)
    self.treatProtons ( tup , b , '_B' ) ## prefix is optional 
    ...    
    Following variables are added into n-tuple:
    - mindll_pK[+suffix]
    minimal value of  DLL(p-K)  for all protons
    - mindll_ppi[+suffix]
    minimal value of  DLL(p-pi) for all protons
    - minann_P[+suffix]
    minimal value of  PROBNNp   for all protons
    - n_proton  Number of protons in the decay
    
    Then for each proton in the decay:
    - p_proton[+suffix]       Proton momentum 
    - pt_proton[+suffix]      Proton transverse momentum 
    - eta_proton[+suffix]     Proton pseudorapidity 
    - hasRich_proton[+suffix] Has it RICH information?(==``Is proton in RICH acceptance?'')
    - pid_proton_pi[+suffix]  PID information: DLL(p-pi) 
    - pid_proton_K[+suffix]   PID information: DLL(p-K) 
    - ann_proton[+suffix]     PID information: PROBNNp   (probability to be a proton) 
    - ann_proton_PI[+suffix]  PID information: PROBNNpi  (probability to be a pion)
    - ann_proton_K[+suffix]   PID information: PROBNNk   (probability to be a kaon)

    For each proton and for each ANNPID tune :
    - annpid_proton[+suffix]_[tune]    PROBBNp     for given tune
    - annpid_proton_PI[+suffix]_[tune] PROBBNpi    for given tune
    - annpid_proton_K[+suffix]_[tune]  PROBBNK     for given tune
    - annpid_proton_GH[+suffix]_[tune] PROBBNghost for given tune
    
    """
    #
    ## initialize it
    #
    if not self.fill_initialized : fill_initialize ( self )
    ## 
    if not self._protons_init_ :
        _fun_ = treatProtons
        logger.info  ( "%s: %s" % ( _fun_.__name__ , _doc_ ( _fun_.__doc__ ) ) ) 
        self._protons_init_ = True
    ## 
    if hasattr ( p , 'particle' ) : p = p.particle() 
    #
    ## get all protons from the decay tree
    #
    good   = LHCb.Particle.ConstVector()
    p.children ( self._protons , good ) 
    #
    _cnt  = self.counter ( '#proton' + suffix )
    _cnt += good.size()
    #
    sc = tup.column_float ( 'mindll_pK'     + suffix , self._min_dll_PK   ( p ) ) 
    sc = tup.column_float ( 'mindll_ppi'    + suffix , self._min_dll_Ppi  ( p ) ) 
    sc = tup.column_float ( 'minann_P'      + suffix , self._min_annpid_P ( p ) ) 
    #
    sc = tup.column_float ( 'minPt_proton'  + suffix , self._min_Pt_p     ( p ) )
    sc = tup.column_float ( 'minP_proton'   + suffix , self._min_P_p      ( p ) )
    sc = tup.column_float ( 'minEta_proton' + suffix , self._min_Eta_p    ( p ) )
    sc = tup.column_float ( 'maxEta_proton' + suffix , self._max_Eta_p    ( p ) )
    #
    if  tunes :
        tunes_  = self.annPidTunes ( 'PROTON' , suffix , tunes )
        for t,pid in tunes_ :
            tup.fArrayP ( 'annpid_proton'    + suffix + '_' + t , pid.PROTON  ,
                          'annpid_proton_PI' + suffix + '_' + t , pid.PION    ,
                          'annpid_proton_K'  + suffix + '_' + t , pid.PROTON  , 
                          'annpid_proton_GH' + suffix + '_' + t , pid.GHOST   , 
                          LHCb.Particle.Range ( good )                        ,
                          'n_proton'         + suffix           , max_protons )
    ##    
    tup.fArrayP (
        'p_proton'       + suffix , P   / GeV      , 
        'pt_proton'      + suffix , PT  / GeV      , 
        'eta_proton'     + suffix , ETA            , 
        'ann_proton'     + suffix , PROBNNp        ,
        LHCb.Particle.Range ( good )               ,
        'n_proton'       + suffix , max_protons    )
    tup.fArrayP (
        'hasRich_proton' + suffix , self._has_rich ,  
        LHCb.Particle.Range ( good )               ,
        'n_proton'       + suffix , 10             )
    return tup.fArrayP (
        'pid_proton_pi'  + suffix , PIDp - PIDpi   ,
        'pid_proton_K'   + suffix , PIDp - PIDK    ,
        'ann_proton_PI'  + suffix , PROBNNpi       ,
        'ann_proton_K'   + suffix , PROBNNk        ,
        LHCb.Particle.Range ( good )               ,
        'n_proton'       + suffix , max_protons    )

# ==============================================================================
## add photon information into n-tuple
#  @code
#  tup = ...  ## n-tuple 
#  B   = ...  ## the particle or loop-object
#  self.treatPhotons  ( tup , B  , '_b'   )  ## use suffix to mark variables 
#  @endcode
# 
# Following variables are added into n-tuple 
#  - <code>minE_gamma[+suffix]</code>        Minimal photon energy
#  - <code>minEt_gamma[+suffix]</code>       Minimal photon transverse energy
#  - <code>minIsNotE_gamma[+suffix]</code>   Minimal value for 'IsNotE'-estimator 
#  - <code>minIsNotH_gamma[+suffix]</code>   Minimal value for 'IsNotH'-estimator 
#  - <code>minPhotonID_gamma[+suffix]</code> Minimal value for 'PhotonID'-estimator 
#  - <code>minIsPhoton_gamma[+suffix]</code> Minimal value for 'IsPhoton'-estimator 
#  - <code>n_photon</code>  Number of photons in the decay
#    
#  Then for each photon in the decay:
#  - <code>e_photon[+suffix]</code>        Photon energy
#  - <code>et_photon[+suffix]</code>       Photon transverse energy
#  - <code>eta_photon[+suffix]</code>      Photon pseudorapidity
#  - <code>tx_photon[+suffix]</code>       value of PX/PZ 
#  - <code>ty_photon[+suffix]</code>       value of PY/PZ 
#  - <code>CL_photon[+suffix]</code>       Photon confidence level (obsolete?)
#  - <code>IsNotE_photon[+suffix]</code>   'IsNotE'-estimator
#  - <code>IsNotH_photon[+suffix]</code>   'IsNotH'-estimator
#  - <code>IsPhoton_photon[+suffix]</code> 'IsPhoton'-estimator
#  - <code>PhotonID_photon[+suffix]</code> 'PhotonID'-estimator
#  - <code>HypoSpdM_photon[+suffix]</code> 'CaloHypoSpdM'-estimator. Cnv vs !Cnv 
#  - <code>seed_photon[+suffix]</code>      CaloCellID for the seed Ecal cell 
#  - <code>area_photon[+suffix]</code>      Calorimeter area for the seed Ecal cell 
#  @param tup   n-tuple
#  @param p     the particle 
def treatPhotons ( self         ,
                   tup          ,
                   p            ,
                   suffix  = '' ) :
    """Add photon information into n-tuple
    ...
    tup = ... ## n-tuple 
    b   = ... ## the particle  (or looping object)
    self.treatPhotons ( tup , b , '_B' ) ## prefix is optional 
    ...        
    Following variables are added into n-tuple:
    - minE_gamma[+suffix]        Minimal photon energy
    - minEt_gamma[+suffix]       Minimal photon transverse energy
    - minIsNotE_gamma[+suffix]   Minimal value for 'IsNotE'-estimator 
    - minIsNotH_gamma[+suffix]   Minimal value for 'IsNotH'-estimator 
    - minPhotonID_gamma[+suffix] Minimal value for 'PhotonID'-estimator 
    - minIsPhoton_gamma[+suffix] Minimal value for 'IsPhoton'-estimator 
    - n_photon  Number of photons in the decay
    
    Then for each photon in the decay:
    - e_photon[+suffix]        Photon energy
    - et_photon[+suffix]       Photon transverse energy
    - eta_photon[+suffix]      Photon pseudorapidity
    - tx_photon[+suffix]       value of PX/PZ 
    - ty_photon[+suffix]       value of PY/PZ 
    - CL_photon[+suffix]       Photon confidence level (obsolete?)
    - IsNotE_photon[+suffix]   'IsNotE'-estimator
    - IsNotH_photon[+suffix]   'IsNotH'-estimator
    - IsPhoton_photon[+suffix] 'IsPhoton'-estimator
    - PhotonID_photon[+suffix] 'PhotonID'-estimator
    - HypoSpdM_photon[+suffix] 'CaloHypoSpdM'-estimator. Cnv vs !Cnv 
    - seed_photon[+suffix]      CaloCellID for the seed Ecal cell 
    - area_photon[+suffix]      Calorimeter area for the seed Ecal cell 
    """
 
    #
    ## initialize it
    #
    if not self.fill_initialized : fill_initialize ( self )
    ## 
    if not self._photons_init_ :
        _fun_ = treatPhotons
        logger.info  ( "%s: %s" % ( _fun_.__name__ , _doc_ ( _fun_.__doc__ ) ) ) 
        self._photons_init_ = True
    ## 
    if hasattr ( p , 'particle' ) : p = p.particle() 
    #
    ## get all protons form decay tree
    #
    good   = LHCb.Particle.ConstVector()
    p.children ( self._gamma , good ) 
    #
    _cnt  = self.counter  ( '#gamma' + suffix )
    _cnt += good.size()
    #
    sc = tup.column_float ( 'minE_gamma'        + suffix , self._min_E_gamma         ( p ) ) 
    sc = tup.column_float ( 'minEt_gamma'       + suffix , self._min_Et_gamma        ( p ) ) 
    sc = tup.column_float ( 'minIsNotE_gamma'   + suffix , self._min_IsNotE_gamma    ( p ) ) 
    sc = tup.column_float ( 'minIsNotH_gamma'   + suffix , self._min_IsNotH_gamma    ( p ) )
    sc = tup.column_float ( 'minPhotonID_gamma' + suffix , self._min_PhotonID_gamma  ( p ) )
    sc = tup.column_float ( 'minIsPhoton_gamma' + suffix , self._min_IsPhoton_gamma  ( p ) )
    #
    tup.fArrayP (
        'e_photon'        + suffix       , P   / GeV          , 
        'et_photon'       + suffix       , PT  / GeV          , 
        'CL_photon'       + suffix       , CL                 ,
        'HypoSpdM_photon' + suffix       , self._CaloHypoSpdM ,        
        LHCb.Particle.Range ( good )     ,
        'n_photon'        + suffix       , max_gamma )
    
    tup.fArrayP (
        'IsNotE_photon'   + suffix       , self._IsNotE   , 
        'IsNotH_photon'   + suffix       , self._IsNotH   , 
        'IsPhoton_photon' + suffix       , self._IsPhoton , 
        'PhotonID_photon' + suffix       , self._PhotonID , 
        LHCb.Particle.Range ( good ) ,
        'n_photon'        + suffix       , max_gamma      )

    ## put into n-tuple the full cell seed and the calo area index 
    v_seed = v_doubles()
    v_area = v_doubles()
    for g in good :
        seed = self._cellID ( g ) 
        v_seed.push_back ( seed.all  () ) ## get all bits 
        v_area.push_back ( seed.area () ) ## get only the area

    tup.farray ( 'seed_gamma' + suffix , v_seed    , 
                 'n_photon'   + suffix , max_gamma )
    tup.farray ( 'area_gamma' + suffix , v_area    , 
                 'n_photon'   + suffix , max_gamma )
    
    return tup.fArrayP (
        'eta_photon'      + suffix       , ETA       , 
        'tx_photon'       + suffix       , PX / PZ   , 
        'ty_photon'       + suffix       , PY / PZ   , 
        LHCb.Particle.Range ( good ) ,
        'n_photon'        + suffix       , max_gamma )

# ==============================================================================
## add di-gamma information into n-tuple
#  @code
#  tup = ...  ## n-tuple 
#  B   = ...  ## the particle or loop-object
#  self.treatDiGammas ( tup , B  , '_b'   )  ## use suffix to mark variables 
#  @endcode
# 
# Following variables are added into n-tuple 
# - <code>n_digamma<code>  Number of digammas in the decay
#    
# Then for each digamma in the decay [0,n_digamma):
# - <code>e_digamma[+suffix]</code>    Di-gamma energy
# - <code>et_digamma[+suffix]</code>   Di-gamma transverse energy
# - <code>ID_digamma[+suffix]</code>   Di-gamma ID 
# - <code>m_digamma[+suffix]</code>    Di-gamma mass 
# - <code>lv01_digamma[+suffix]</code> Di-gamma decay angle LV01 
# @param tup   n-tuple
# @param p     the particle 
def treatDiGammas ( self         ,
                   tup          ,
                   p            ,
                   suffix  = '' ) :
    """Add di-gamma information into n-tuple
    ...
    tup = ... ## n-tuple 
    b   = ... ## the particle  (or looping object)
    self.treatDiGammas ( tup , b , '_B' ) ## prefix is optional 
    ...            
    Following variables are added into n-tuple:
    - n_digamma  Number of digammas in the decay
    
    Then for each digamma in the decay [0,n_digamma):
    - e_digamma[+suffix]       Di-gamma energy
    - et_digamma[+suffix]      Di-gamma transverse energy
    - ID_digamma[+suffix]      Di-gamma ID 
    - m_digamma[+suffix]       Di-gamma mass 
    - lv01_digamma[+suffix]    Di-gamma decay angle LV01 
    """
    
    #
    ## initialize it
    #
    if not self.fill_initialized : fill_initialize ( self )
    ##
    if not self._digammas_init_ :
        _fun_ = treatDiGammas
        logger.info  ( "%s: %s" % ( _fun_.__name__ , _doc_ ( _fun_.__doc__ ) ) ) 
        self._digammas_init_ = True
    ## 
    if hasattr ( p , 'particle' ) : p = p.particle() 
    #
    ## get all di-gammas from decay tree
    #
    good   = LHCb.Particle.ConstVector()
    p.children ( self._digamma , good ) 
    #
    _cnt  = self.counter ( '#digamma' + suffix )
    _cnt += good.size()
    #
    tup.fArrayP (
        'e_digamma'          + suffix       , P   / GeV   , 
        'et_digamma'         + suffix       , PT  / GeV   , 
        'ID_digamma'         + suffix       , ID          ,
        LHCb.Particle.Range ( good ) ,
        'n_digamma'          + suffix       , max_digamma )

    return tup.fArrayP (
        'm_digamma'          + suffix       , M   / GeV   , 
        'lv01_digamma'       + suffix       , LV01        ,
        LHCb.Particle.Range ( good ) ,
        'n_digamma'          + suffix       , max_digamma )


# ==============================================================================
## ditto 
treatDiPhotons = treatDiGammas 

# ==============================================================================
## add muon information into n-tuple
#  @code
#  tup = ...  ## n-tuple 
#  B   = ...  ## the particle or loop-object
#  self.treatMuons  ( tup , B  , '_b' )  ## use suffix to mark variables 
#  @endcode
# 
# Following variables are added into n-tuple 
# - <code>mindll_mu[+suffix]</code>
# minimal value of  DLL(mu-pi)  for all muons
# - <code>minann_mu[+suffix]</code>
# minimal value of  PROBNNmu   for all muons
# - <code>n_muon</code>  Number of muons in the decay
#    
# Then for each muon in the decay:
# - <code>p_mu[+suffix]</code>        Muon momentum 
# - <code>pt_mu[+suffix]</code>       Muon transverse momentum 
# - <code>eta_mu[+suffix]</code>      Muon pseudorapidity
# - <code>eEcal_mu[+suffix]</code>    Energy deposited in Ecal 
# - <code>eHcal_mu[+suffix]</code>    Energy deposited in Hcal 
# - <code>pid_mu[+suffix]</code>      PID information: DLL(mu-pi) for muon
# - <code>ann_mu[+suffix]</code>      PID information: PROBNNmu for muon 
# - <code>flag_mu[+suffix]</code>     Flag(1*HasMuon+10*IsMuonLoose+100*IsMuon+1000*IsMuonTight) for muons
# - <code>nshared_mu[+suffix]</code>  Number of shared muon hits (LHCb::MuonPID::nShared)
#
# For each muon and for each ANNPID tune :
# - <code>annpid_muon[+suffix]_[tune]</code>    PROBBNmu    for given tune
# - <code>annpid_muon_PI[+suffix]_[tune]</code> PROBBNpi    for given tune
# - <code>annpid_muon_K[+suffix]_[tune]</code>  PROBBNK     for given tune
# - <code>annpid_muon_GH[+suffix]_[tune]</code> PROBBNghost for given tune
#
#  @see LHCb::MuonPID::nShared 
#  @param tup   n-tuple
#  @param p     the particle 
def treatMuons ( self         ,
                 tup          ,
                 p            ,
                 suffix  = '' ,
                 tunes   = () ) :
    """Add muon information into n-tuple
    ...
    tup = ... ## n-tuple 
    b   = ... ## the particle  (or looping object)
    self.treatMuons ( tup , b , '_B' ) ## prefix is optional 
    ...            
    Following variables are added into n-tuple:
    - mindll_mu[+suffix]
    minimal value of  DLL(mu-pi) for all muons
    - minann_mu[+suffix]
    minimal value of  PROBNNmu   for all muons
    - n_muon  Number of muons in the decay
    
    Then for each muon in the decay:
    - p_mu[+suffix]       Muon momentum 
    - pt_mu[+suffix]      Muon transverse momentum 
    - eta_mu[+suffix]     Muon pseudorapidity 
    - eEcal_mu[+suffix]   energy deposited in Ecal
    - eHcal_mu[+suffix]   energy deposited in Hcal  
    - pid_mu[+suffix]     PID information: DLL(mu-pi)
    - ann_mu[+suffix]     PID information: PROBNNmu  (probability to be a muon)
    - flag_mu[+suffix]    Flag(1*HasMuon+10*IsMuonLoose+100*IsMuon+1000*IsMuonTight) for muons
    - nshared_mu[+suffix] Number of shared muon hits (LHCb::MuonPID::nShared)

    For each muon and for each ANNPID tune :
    - annpid_muon[+suffix]_[tune]      PROBBNmu    for given tune
    - annpid_muon_PI[+suffix]_[tune]   PROBBNpi    for given tune
    - annpid_muon_K[+suffix]_[tune]    PROBBNK     for given tune
    - annpid_proton_GH[+suffix]_[tune] PROBBNghost for given tune
    
    """
    
    #
    ## initialize it
    #
    if not self.fill_initialized : fill_initialize ( self )
    ## 
    if not self._muons_init_ :
        _fun_ = treatMuons
        logger.info  ( "%s: %s" % ( _fun_.__name__ , _fun_.__doc__ ) )
        self._muons_init_ = True
    ## 
    if hasattr ( p , 'particle' ) : p = p.particle() 
    #
    ## get all muons from decay tree
    #
    good    = LHCb.Particle.ConstVector()
    p.children ( self._muons , good ) 
    #
    _cnt  = self.counter ( '#muon' + suffix )
    _cnt += good.size()
    #
    sc = tup.column_float ( 'mindll_mu'   + suffix , self._min_dll_Mu    ( p ) ) 
    sc = tup.column_float ( 'minann_mu'   + suffix , self._min_annpid_Mu ( p ) ) 
    #
    sc = tup.column_float ( 'minPt_mu'    + suffix , self._min_Pt_mu     ( p ) )
    sc = tup.column_float ( 'minP_mu'     + suffix , self._min_P_mu      ( p ) )
    sc = tup.column_float ( 'minEta_mu'   + suffix , self._min_Eta_mu    ( p ) )
    sc = tup.column_float ( 'maxEta_mu'   + suffix , self._max_Eta_mu    ( p ) )
    #
    if  tunes :
        tunes_  = self.annPidTunes ( 'MUON'  , suffix , tunes )
        for t , pid in tunes_ :
            tup.fArrayP ( 'annpid_muon'    + suffix  + '_' + t , pid.MUON   ,
                          'annpid_muon_PI' + suffix  + '_' + t , pid.PION   ,
                          'annpid_muon_K'  + suffix  + '_' + t , pid.KAON   , 
                          'annpid_muon_GH' + suffix  + '_' + t , pid.GHOST  , 
                          LHCb.Particle.Range ( good )                      ,
                          'n_muon'         + suffix            , max_muons  )
    ##    
    sc = tup.fArrayP   (
        'p_mu'       + suffix , P   / GeV       ,  
        'pt_mu'      + suffix , PT  / GeV       , 
        'eta_mu'     + suffix , ETA             , 
        LHCb.Particle.Range ( good )            ,
        'n_muon'     + suffix ,  max_muons      )
    sc = tup.fArrayP (
        'flag_mu'    + suffix , self._flagmu    , 
        'nshared_mu' + suffix , self._nsharedmu ,
        LHCb.Particle.Range ( good )            ,
        'n_muon'     + suffix ,  max_muons      )
    return tup.fArrayP (
        'eEcal_mu'   + suffix , self._EcalE     ,
        'eHcal_mu'   + suffix , self._HcalE     ,
        'pid_mu'     + suffix , PIDmu - PIDpi   ,  
        'ann_mu'     + suffix , PROBNNmu        ,  
        LHCb.Particle.Range ( good )            ,
        'n_muon'     + suffix , max_muons       )

# ==============================================================================
## add tracks information into n-tuple
#  @code
#  tup = ...  ## n-tuple 
#  B   = ...  ## the particle or loop-object
#  self.treatTracks  ( tup , B  , '_b'   )  ## use suffix to mark variables 
#  @endcode
# 
# Following variables are added into n-tuple 
#   The method adds track-specific information into n-tuple
#   - <code>deltaM2_min_track_ss/os[+suffix]</code>
#   Minimal value of <code>delta_m2(track1, track2)<code>
#   for all pairs of same-sign (<code>_ss</code>)
#   and opposite sign(<code>_os</code>) tracks,
#   where function <code>deltaM2</code> is defined as
#   \f[  \left. \Delta M^2\right|_{min}(p_1,p_2) =
#   \frac{ (p_1+p_2)^2 - 2p^2_1 - 2p^2_2 } { (p_1+p_2)^2} \f] 
#   see <code>LoKi::Kinematics::deltaM2</code>
#   - <code>deltaAlpha_min_track_ss/os[+suffix]</code>
#   Minimal value of th eangle between two momenta for all pairs of same-sign (``_ss'')
#   and opposite sign ``_os'' tracks
#   see  LoKi::Kinematics::deltaAlpha
#   - <code>overlap_max_track_ss/os[+suffix]</code>
#   Maximal value ``overlap'' for all pairs of same-sign (``_ss'')
#   and opposite sign ``_os'' tracks
#   ``Overlap'' is defined as fraction of common/shared hits between two tracks 
#   see LHCb::HasIDs::overlap 
#   - <code>minPt_track[+suffix]</code>
#   Minimal transverse momentum of the tracks
#   - <code>min/maxEta_track[+suffix]</code>
#   Minimal/maxima eta/pseudorapidity of the tracks
#   - <code>maxChi2_track[+suffix]</code>
#   Maximal chi2/ndf for the track
#   - <code>minKL_track[+suffix]</code>
#   Minimal value of Kullback-Leibler divergency for the tracks
#   - <code>maxTrGh_track[+suffix]</code>
#   Maximal value of Track Ghost probability for the tracks (track-based)
#   - <code>maxAnnGh_track[+suffix]</code>
#   Maximal value of       Ghost probability for the tracks (PID-based)
#   - <code>n_track[+suffix]</code> Number of tracks in the decay
#
#  And then for each track in the decay:
#   - <code>p_track[+suffux]</code>     momentum of the track
#   - <code>pt_track[+suffux]</code>    transverse momentum of the track
#   - <code>eta_track[+suffux]</code>   eta/pseudorapidity  of the track
#   - <code>phi_track[+suffux]</code>   phi (azimuth angle) of the track
#   - <code>chi2_track[+suffux]</code>  chi2/ndf of the track
#   - <code>PChi2_track[+suffux]</code> fit probability calculated from chi2/ndf of the track
#   - <code>ann_track[+suffix]</code>   Ghost probability (PID-based)
#   - <code>trgh_track[+suffix]</code>  Track Ghost probability (Track-based)
#
#  @see LoKi::Kinematics::deltaM2
#  @see LoKi::Kinematics::deltaAlpha
#  @see LHCb::HashIDs::overlap 
#  @see PROBNNghost
#  @see TRFHOSTPROB
#  @param tup   n-tuple
#  @param p     the particle 
def treatTracks ( self         ,
                  tup          ,
                  p            ,
                  suffix  = '' ) :
    """The method adds track-specific information into n-tuple
    ...
    tup = ... ## n-tuple 
    b   = ... ## the particle  (or looping object)
    self.treatTracks ( tup , b , '_B' ) ## prefix is optional 
    ...                
    Following variables are added into n-tuple:
    - deltaM2_min_track_ss/os[+suffix]:
    Minimal value of delta_m2(track1, track2) for all pairs of same-sign (``_ss'')
    and opposite sign ``_os'' tracks, where function minm2 is
    delta_M2(p1,p2) = (m^2(p1+p2) - 2*m^2(p1)-2*m^2(p2) )/m^2(p1+p2)
    see  LoKi::Kinematics::deltaM2
    - deltaAlpha_min_track_ss/os[+suffix]:
    Minimal value of th eangle between two momenta for all pairs of same-sign (``_ss'')
    and opposite sign ``_os'' tracks
    see  LoKi::Kinematics::deltaAlpha
    - overlap_max_track_ss/os[+suffix]:
    Maximal value ``overlap'' for all pairs of same-sign (``_ss'')
    and opposite sign ``_os'' tracks
    ``Overlap'' is defined as fraction of common/shared hits between two tracks 
    see LHCb::HasIDs::overlap 
    - minPt_track[+suffix]
    Minimal pT of the tracks
    - min/maxEta_track[+suffix]
    Minimal/maxima eta/pseudorapidity of the tracks
    - maxChi2_track[+suffix]
    Maximal chi2/ndf for the track
    - minKL_track[+suffix]
    Minimal value of Kullback-Leibler divergency for the tracks
    - maxTrGh_track[+suffix]
    Maximal value of Track Ghost probability for the tracks (track-based)
    - maxAnnGh_track[+suffix]
    Maximal value of       Ghost probability for the tracks (PID-based)
    - n_track[+suffix] Number of tracks in the decay
    
    And then for each track in the decay:
    - p_track[+suffux]     momentum of the track
    - pt_track[+suffux]    transverse momentum of the track
    - eta_track[+suffux]   eta/pseudorapidity  of the track
    - phi_track[+suffux]   phi (azimuth angle) of the track
    - chi2_track[+suffux]  chi2/ndf of the track
    - PChi2_track[+suffux] fit probability calculated from chi2/ndf of the track
    - ann_track[+suffix]   Ghost probability (PID-based)
    - trgh_track[+suffix]  Track Ghost probability (Track-based)
    """
    #
    ## initialize it
    #
    if not self.fill_initialized : fill_initialize ( self )
    ## 
    if not self._tracks_init_ :
        _fun_ = treatTracks
        logger.info  ( "%s: %s" % ( _fun_.__name__ , _doc_ ( _fun_.__doc__ ) ) ) 
        self._tracks_init_ = True
    ## 
    if hasattr ( p , 'particle' ) : p = p.particle() 
    #
    ## get all tracks from the decay tree
    #
    from math import sqrt, acos 
    #
    #
    good   = LHCb.Particle.ConstVector()
    p.children ( self._tracks , good ) 
    #
    _cnt  = self.counter ( '#track' + suffix )
    _cnt += good.size()
    #
    ## calcualate the minimal mass variable for: 
    m2min_ss  =  1.e+6 ## 1) same     sign tracks 
    m2min_os  =  1.e+6 ## 2) opposite sign tracks
    damin_ss  =  1.e+6 ## 1) same     sign tracks 
    damin_os  =  1.e+6 ## 2) opposite sign tracks
    ovmax_os  = -1.e+6 ## 1) same     sign tracks 
    ovmax_ss  = -1.e+6 ## 2) opposite sign tracks 
    for i in range ( 0 , good.size() ) :
        p_i = good[i]
        q_i = Q ( p_i )
        v_i = p_i.momentum()
        
        for j in range ( i + 1 , good.size() ) :
            p_j = good [j]
            q_j = Q ( p_j )
            v_j = p_j.momentum()
            
            m2    = self._delta_m2    ( v_i , v_j , self.pion_mass )
            da    = self._delta_alpha ( p_i , p_j )
            r     = self._overlap     ( p_i , p_j ) 
            ov    = max ( r.first , r.second )
            
            if q_i * q_j < 0 :
                m2min_os = min ( m2min_os , m2 )
                damin_os = min ( damin_os , da )
                ovmax_os = max ( ovmax_os , ov )
            else             :
                m2min_ss = min ( m2min_ss , m2 ) 
                damin_ss = min ( damin_ss , da ) 
                ovmax_ss = max ( ovmax_ss , ov )
                
    if m2min_ss <  1.e+6 : sc = tup.column_float ( 'deltaM2_min_track_ss'     + suffix , m2min_ss )    
    if m2min_os <  1.e+6 : sc = tup.column_float ( 'deltaM2_min_track_os'     + suffix , m2min_os )
    if damin_ss <  1.e+6 : sc = tup.column_float ( 'deltaAlpha_min_track_ss'  + suffix , damin_ss )    
    if damin_os <  1.e+6 : sc = tup.column_float ( 'deltaAlpha_min_track_os'  + suffix , damin_os )
    if ovmax_ss > -1.e+6 : sc = tup.column_float ( 'overlap_max_track_ss'     + suffix , ovmax_ss )    
    if ovmax_os > -1.e+6 : sc = tup.column_float ( 'overlap_max_track_os'     + suffix , ovmax_os )
    
    sc = tup.column_float ( 'minPt_track'     + suffix , self._min_Pt          ( p ) )
    sc = tup.column_float ( 'minEta_track'    + suffix , self._min_Eta         ( p ) )
    sc = tup.column_float ( 'maxEta_track'    + suffix , self._max_Eta         ( p ) )
    sc = tup.column_float ( 'maxChi2_track'   + suffix , self._maxTrChi2       ( p ) )
    sc = tup.column_float ( 'maxTrGh_track'   + suffix , self._maxTrGhost      ( p ) )
    sc = tup.column_float ( 'minKL_track'     + suffix , self._minTrKL         ( p ) )
    sc = tup.column_float ( 'maxAnnGh_track'  + suffix , self._max_anngh_track ( p ) )
    
    tup.fArrayP (
        'p_track'     + suffix , P   / GeV   , 
        'pt_track'    + suffix , PT  / GeV   , 
        'eta_track'   + suffix , ETA         , 
        'phi_track'   + suffix , PHI         ,                        
        LHCb.Particle.Range ( good )         , 
        'n_track'     + suffix , max_tracks  )
    
    return tup.fArrayP (
        'chi2_track'  + suffix , TRCHI2DOF   ,
        'PChi2_track' + suffix , TRPCHI2     , 
        'ann_track'   + suffix , PROBNNghost ,                        
        'trgh_track'  + suffix , TRGHOSTPROB ,                        
        LHCb.Particle.Range ( good )         , 
        'n_track'     + suffix , max_tracks  )

# =============================================================================
## add basic kinematical information into N-tuple
#  @code
#  tup = ...  ## n-tuple 
#  B   = ...  ## the particle or loop-object
#  self.treatKine  ( tup , B  , '_b'   )  ## use suffix to mark variables 
#  @endcode
# 
# Following variables are added into n-tuple 
# - <code>pid[+suffix]</code>      particle ID
# - <code>pt[+suffix]</code>       particle transverse momentum 
# - <code>m[+suffix]</code>        particle mass 
# - <code>eta[+suffix]</code>      particle pseudirapidity 
# - <code>phi[+suffix]</code>      particle azimuth angle phi 
# - <code>p4[+suffix]</code>       particle 4-momentum    
# - <code>c2ip[+suffix]</code>     chi2_IP with respect to associated primary vertex         ; BPVIPCHI2()
# For composed particles :
# - <code>y[+suffix]</code>        particle rapidity
# - <code>lv01[+suffix]</code>     particle decay angle (cosine)                             ; LV01
# For particles with valid end-vertex :
# - <code>ctau[+suffix]</code>     particle c*tau                                            ; BPVLTIME()*c_light 
# - <code>ctau9[+suffix]</code>    particle c*tau if chi2_IP<9, otherwise negative infinity  ; BPVLTIME(9)*c_light 
# - <code>ctau25[+suffix]</code>   particle c*tau if chi2_IP<25, otherwise negative infinity ; BPVLTIME(25)*c_light  
# - <code>dtf[+suffix]</code>      chi2_DTF/ndf from Decay Tree Refit with PV constraint     ; DTF_CHI2NDOF( True ) 
# - <code>dls[+suffix]</code>      decay length significance                                 ; BPVDLS
# - <code>tz[+suffix]</code>       t_z with respect to the associated primary vertex         ; BPVTZ  
# - <code>vchi2[+suffix]</code>    Vertex chi2
# - <code>vchi2ndf[+suffix]</code> Vertex chi2/ndf
# - <code>good[+suffix]</code>     The boolean result of result of good(p)
def treatKine ( self                      ,
                tup                       ,
                p                         ,
                suffix                    ,
                good   = lambda s : False ) :
    """Add basic kinematical information into N-tuple
    ...
    tup = ... ## n-tuple 
    b   = ... ## the particle  (or looping object)
    self.treatKine ( tup , b , '_B' ) ## prefix is optional 
    ...                    
    Following variables are added into n-tuple:
    - pid[+suffix]      particle ID
    - pt[+suffix]       particle transverse momentum 
    - m[+suffix]        particle mass 
    - eta[+suffix]      particle pseudirapidity 
    - phi[+suffix]      particle azimuth angle phi 
    - p4 [+suffix]      particle 4-momentum 
    For composed particles :
    - y[+suffix]        particle rapidity
    - lv01[+suffix]     particle decay angle (cosine)                             ; LV01
    If has at least two track
    - c2ip[+suffix]     chi2_IP with respect to associated primary vertex         ; BPVIPCHI2()
    
    For particles with valid end-vertex :
    - ctau[+suffix]     particle c*tau                                            ; BPVLTIME()*c_light
    - ctau9[+suffix]    particle c*tau if chi2_ip<9, otherwise negative infinity  ; BPVLTIME(9)*c_light 
    - ctau25[+suffix]   particle c*tau if chi2_ip<25, otherwise negative infinity ; BPVLTIME(25)*c_light 
    - dtf[+suffix]      chi2_DTF/ndf from Decay Tree Refit with PV constraint     ; DPF_CHI2NDOF( True ) 
    - dls[+suffix]      decay length significance                                 ; BPVDLS 
    - vchi2[+suffix]    Vertex chi2
    - vchi2ndf[+suffix] Vertex chi2/ndf
    - good[+suffix]     The boolean result of result of good(p)  
    """
    #
    ## initialize it
    #
    if not self.fill_initialized : fill_initialize ( self )
    ##
    if not self._kine_init_ :
        _fun_ = treatKine
        logger.info  ( "%s: %s" % ( _fun_.__name__ , _doc_ ( _fun_.__doc__ ) ) ) 
        self._kine_init_ = True
    ## 
    if hasattr ( p , 'particle' ) : p = p.particle() 
    ##
    tup.column_int   ( 'pid'   + suffix , int ( ID      ( p ) )     )
    tup.column_float ( 'pt'    + suffix ,       PT      ( p ) / GeV )
    tup.column_float ( 'm'     + suffix ,        M      ( p ) / GeV )
    tup.column_float ( 'eta'   + suffix ,      ETA      ( p )       )
    tup.column_float ( 'phi'   + suffix ,      PHI      ( p )       )
    #
    ## 4-vector
    #
    tup.column       ( 'p4'    + suffix , p.momentum() / GeV  )
    
    if self._ok_for_ip ( p ) :
        tup.column_float ( 'c2ip'  + suffix , self._ipchi2  ( p ) )
    ##
    if not p.isBasicParticle() :
        
        tup.column_float ( 'y'     + suffix , Y          ( p ) )
        tup.column_float ( 'lv01'  + suffix , self._lv01 ( p ) )                
            
        if p.endVertex() :
            
            tup.column_float ( 'ctau'    + suffix , self._ctau     ( p ) )
            tup.column_float ( 'ctau9'   + suffix , self._ctau_9   ( p ) )
            tup.column_float ( 'ctau25'  + suffix , self._ctau_25  ( p ) )
            tup.column_float ( 'dtf'     + suffix , self._dtfchi2  ( p ) )
            tup.column_float ( 'dls'     + suffix , self._dls      ( p ) )
            tup.column_float ( 'vchi2'   + suffix , self._vchi2    ( p ) )
            tup.column_float ( 'vchi2ndf'+ suffix , self._vchi2ndf ( p ) )
            tup.column_float ( 'vchi2ndf'+ suffix , self._vchi2ndf ( p ) )
            tup.column_float ( 'tz'      + suffix , self._tz       ( p ) )
        
    ##
    ok = True 
    if good : ok = good ( p )
    
    tup.column_bool ( 'good' + suffix ,  ok )
        
    return SUCCESS

# =============================================================================
## fill information about masses for all combinations of daughter particles
#  @code
#  tup = ... ## n-tuple 
#  b   = ... ## particle or looping object
#  ## fill mIJ, mIJK, mIJKL, ... variables for all combinations of I,J,K,L,...
#  self.fillMasses ( tup , b                ) 
#  ## use suffix for variable names: 
#  self.fillMasses ( tup , b , 'c1'         )
#  ## refit decay tree using DTF_FUN with PV-constraint
#  self.fillMasses ( tup , b , 'c2' , True  )
#  ## refit decay tree using DTF_FUN with PV-constraint and constrain for J/psi mass:
#  self.fillMasses ( tup , b , 'c3' , True  , 'J/psi(1S)' )
# @endcode
# 
# Following variables are added into n-tuple:
# - <code>mIJ[+suffix]</code>      : the mass of all pairwise combinations of daughters, I<J 
# - <code>mIJK[+suffix]</code>     : the mass of all triplets of daughters, I<J<K 
# - <code>mIJKL[+suffix]</code>    : the mass of combinations of four daughters, I<J<K<L
# - ...
# - <code>mIJKLMNO[+suffix]</code> : the mass of combinations of seven daughters I<J<K<L<M<N<O
# @attention: indices starts from 1
def fillMasses ( self        ,
                 tup         ,
                 b           ,
                 suffix      ,
                 *args       ) :
    """Fill information about mass of all sub-combinations of daughter particles
    ...
    tup = ... ## n-tuple 
    b   = ... ## particle or looping object
    # - fill mIJ, mIJK, mIJKL, ... variables for all combinations of I,J,K,L,...
    self.fillMasses ( tup , b                ) 
    # - use suffix for variable names: 
    self.fillMasses ( tup , b , 'c1'         )
    # -  refit decay tree using DTF_FUN with PV-constraint
    self.fillMasses ( tup , b , 'c2' , True  )
    # - refit decay tree using DTF_FUN with PV-constraint and constrain for J/psi mass:
    self.fillMasses ( tup , b , 'c3' , True  , 'J/psi(1S)' )
    ...
    Following variables are added into n-tuple:
    - mIJ[+suffix] : the mass of all pairwise combinations of daughters, I<J 
    - mIJK[+suffix] : the mass of all triplets of daughters, I<J<K 
    - mIJKL[+suffix] : the mass of combinations of four daughters, I<J<K<L
    ...
    - mIJKLMNO[+suffix]: the mass of combinations of seven daughters I<J<K<L<M<N<O
    Important: indices starts from 1
    """
    #
    ## initialize it
    #
    if not self.fill_initialized : fill_initialize ( self )
    ##
    if not self._masses_init_ :
        _fun_ = fillMasses
        logger.info  ( "%s: %s" % ( _fun_.__name__ , _doc_ ( _fun_.__doc__ ) ) ) 
        self._masses_init_ = True
    ## 
    if hasattr ( b , 'particle' ) : b = b.particle() 
    ##
  
    if not self._masses.has_key (suffix) : self._masses[suffix] = {}
        
    nc = b.nChildren()
        
    _ms = self._masses[suffix]
    
    nc = b.nChildren()

    if not _ms :

        ##
        for i in range ( 1 , nc + 1 )  :
            
            bi = b(i)
            if bi and not bi.isBasicParticle() : 
                k1 = "m%d" % i 
                m1 = MASS  ( i )
                if args : m1 = DTF_FUN ( self , m1 , *args )
                _ms [ k1 + suffix ]  =          m1

            ##
            for j in range ( i + 1 , nc + 1 ) :
                
                k2 = "m%d%d" % ( i , j )
                m2 =   MASS    ( i , j )
                if args : m2 = DTF_FUN ( self , m2 , *args )
                _ms [ k2 + suffix ]  =          m2

                ##
                for k in range ( j + 1 , nc + 1 ) :
                    
                    k3 = "m%d%d%d" % ( i , j , k )
                    m3 =     MASS    ( i , j , k )
                    if args : m3 = DTF_FUN ( self , m3 , *args )
                    _ms [ k3 + suffix ]  =          m3

                    ## 
                    for l in range ( k + 1 , nc + 1 ) :
                        
                        k4 = "m%d%d%d%d" % ( i , j , k , l )
                        m4 =       MASS    ( i , j , k , l )
                        if args : m4 = DTF_FUN ( self , m4 , *args )
                        _ms [ k4 + suffix ]  =          m4
                        
                        ## 
                        for m in range ( l + 1 , nc + 1 ) :
                            
                            k5 = "m%d%d%d%d%d" % ( i , j , k , l , m )
                            
                            _ind5 = LoKi.Particles.InvariantMass.Indices ()
                            
                            _ind5 . push_back ( i )
                            _ind5 . push_back ( j )
                            _ind5 . push_back ( k )
                            _ind5 . push_back ( l )
                            _ind5 . push_back ( m )
                            
                            m5 = MASS     ( _ind5 )
                            if args : m5 = DTF_FUN ( self , m5 , *args )
                            _ms [ k5 + suffix ]  =          m5
                            
                            for n in range ( m + 1 , nc + 1 ) :
                                
                                k6 = "m%d%d%d%d%d%d" % ( i , j , k , l , m , n )
                                
                                _ind6 = LoKi.Particles.InvariantMass.Indices ()
                                
                                _ind6 . push_back ( i )
                                _ind6 . push_back ( j )
                                _ind6 . push_back ( k )
                                _ind6 . push_back ( l )
                                _ind6 . push_back ( m )
                                _ind6 . push_back ( n )
                                
                                m6 = MASS     ( _ind6 )
                                if args : m6 = DTF_FUN ( self , m6 , *args )
                                _ms [ k6 + suffix ]  =          m6
                                
                                for o in range ( n + 1 , nc + 1 ) :
                                    
                                    k7 = "m%d%d%d%d%d%d%d" % ( i , j , k , l , m , n , o )
                                
                                    _ind7 = LoKi.Particles.InvariantMass.Indices ()
                                    
                                    _ind7 . push_back ( i )
                                    _ind7 . push_back ( j )
                                    _ind7 . push_back ( k )
                                    _ind7 . push_back ( l )
                                    _ind7 . push_back ( m )
                                    _ind7 . push_back ( n )
                                    _ind7 . push_back ( o )
                                    
                                    m7 = MASS     ( _ind7 )
                                    if args : m7 = DTF_FUN ( self , m7 , *args )
                                    _ms [ k7 + suffix ]  =          m7

                                
    ## finally fill n-tuple 
    for k in _ms :
        tup.column_float ( k , _ms [ k ] ( b ) / GeV )

    return SUCCESS

# =============================================================================
## Add some event summary information
#  @see LHCb::RecSummary
def addRecSummary ( self  ,
                    tup   ,
                    data  ) :
    """ Add event summary information from LHCb::RecSummary     
    ...
    tup          = ... ## n-tuple 
    rec_summary  = ...
    self.addRecSummary( tup , rec_summary )
    ...                    
    Following variables are added into n-tuple:
    - nPV     : number of PVs 
    - nLong   : number of Long       tracks 
    - nDown   : number of Downstream tracks 
    - nUp     : number of Upstream   tracks 
    - nVelo   : number of Velo       tracks 
    - nTT     : number of TT         tracks 
    - nBack   : number of Backward   tracks 
    - nTracks : number of tracks
    - hSPD    : number of SPD hits 
    - hRich1  : number of hits in Rich1 
    - hRich2  : number of hits in Rich2 
    - hVelo   : number of clusters in Velo
    - hIT     : number of clusters in IT 
    - hTT     : number of clusters in TT
    - hOT     : number of clusters in OT
    """
    #
    if not getattr ( self , '_recinfo_init_' , False ) : 
        _fun_ = addRecSummary
        logger.info  ( "%s: %s" % ( _fun_.__name__ , _doc_ ( _fun_.__doc__ ) ) ) 
        self._recinfo_init_ = True
        
    ##
    if data and hasattr ( data , 'summaryData' ) :
        data = data.summaryData()
    
    nPV     = int ( data ( LHCb.RecSummary.nPVs               ) ) 
    nLong   = int ( data ( LHCb.RecSummary.nLongTracks        ) )
    nDown   = int ( data ( LHCb.RecSummary.nDownstreamTracks  ) ) 
    nUp     = int ( data ( LHCb.RecSummary.nUpstreamTracks    ) ) 
    nVelo   = int ( data ( LHCb.RecSummary.nVeloTracks        ) ) 
    nTT     = int ( data ( LHCb.RecSummary.nTTracks           ) ) 
    nBack   = int ( data ( LHCb.RecSummary.nBackTracks        ) ) 
    nTracks = int ( data ( LHCb.RecSummary.nTracks            ) ) 
    #
    hRich1  = int ( data ( LHCb.RecSummary.nRich1Hits         ) ) 
    hRich2  = int ( data ( LHCb.RecSummary.nRich2Hits         ) )
    hVelo   = int ( data ( LHCb.RecSummary.nVeloClusters      ) ) 
    hIT     = int ( data ( LHCb.RecSummary.nITClusters        ) ) 
    hTT     = int ( data ( LHCb.RecSummary.nTTClusters        ) ) 
    hOT     = int ( data ( LHCb.RecSummary.nOTClusters        ) ) 
    hSPD    = int ( data ( LHCb.RecSummary.nSPDhits           ) ) 
    
    tup.column ( 'nPV'       , nPV      , 0 ,    30 )
    tup.column ( 'nLong'     , nLong    , 0 ,  1000 ) 
    tup.column ( 'nDown'     , nDown    , 0 ,   500 ) 
    tup.column ( 'nUp'       , nUp      , 0 ,   500 ) 
    tup.column ( 'nVelo'     , nVelo    , 0 ,  1000 ) 
    tup.column ( 'nTT'       , nTT      , 0 ,   500 ) 
    tup.column ( 'nBack'     , nBack    , 0 ,   200 ) 
    tup.column ( 'nTracks'   , nTracks  , 0 ,  2000 ) 
    
    tup.column ( 'hSPD'      , hSPD     , 0 ,  2000 ) 
    tup.column ( 'hRich1'    , hRich1   , 0 , 30000 ) 
    tup.column ( 'hRich2'    , hRich2   , 0 , 30000 ) 
    tup.column ( 'hVelo'     , hVelo    , 0 , 60000 ) 
    tup.column ( 'hIT'       , hIT      , 0 , 30000 ) 
    tup.column ( 'hTT'       , hTT      , 0 , 30000 ) 
    tup.column ( 'hOT'       , hOT      , 0 , 30000 ) 
    
    return SUCCESS

# =============================================================================
## add some event summary information 
def addGecInfo  ( self  ,
                  tup   ,
                  data  ) :
    """Add global event counters 
    ...
    tup          = ... ## n-tuple 
    data         = ...
    self.addGecInfo ( tup , data )
    ...                    
    Following variables are added into n-tuple:

    """
    #
    if not getattr ( self , '_gecinfo_init_' , False ) : 
        _fun_ = addGecInfo
        logger.info  ( "%s: %s" % ( _fun_.__name__ , _doc_ ( _fun_.__doc__ ) ) ) 
        self._gecinfo_init_ = True
    ## 
    for key in data :
        
        v = int ( data[key] )
        
        tup.column_int ( key + '_gec', v )
        
    return SUCCESS


# =============================================================================
## decorate 
LoKi.Algo.treatPions       =  treatPions
LoKi.Algo.treatKaons       =  treatKaons
LoKi.Algo.treatProtons     =  treatProtons
LoKi.Algo.treatPhotons     =  treatPhotons
LoKi.Algo.treatGammas      =  treatPhotons ## ditto 
LoKi.Algo.treatMuons       =  treatMuons
LoKi.Algo.treatTracks      =  treatTracks
LoKi.Algo.treatKine        =  treatKine

LoKi.Algo.treatDiGamma     =  treatDiGammas
LoKi.Algo.treatDiGammas    =  treatDiGammas ## ditto 
LoKi.Algo.treatDiPhotons   =  treatDiGammas ## ditto 

LoKi.Algo.fillMasses       =  fillMasses
LoKi.Algo.annPidTunes      =  annPidTunes 

LoKi.Algo.addRecSummary    =  addRecSummary 
LoKi.Algo.addGecInfo       =  addGecInfo 

LoKi.Algo.fill_initialize  = fill_initialize
LoKi.Algo.fill_finalize    = fill_finalize

## class attribute 
LoKi.Algo.fill_initialized = False ## class attribute 
LoKi.Algo.fill_finalized   = False ## class attribute 

        
# =============================================================================
## insert "fill" finalization into the global finalization
def decorateFill ( ALGO ) :
    "Insert ``fill'' finalization into the global finalization"
    if hasattr ( ALGO , '_prev_fill_finalize_' ) : return      ## SKIP IT!
    # =========================================================================
    ## updated finalization 
    def _algo_fill_finalize_ ( algorithm ) :
        """Finalize ``specific'' stuff related BenderTools.Fill module
        and then continue with the base-class finalization
        """
        if algorithm.fill_initialized :
            algorithm.Print( 'Finalize "Fill"-machinery' , SUCCESS , 2 )
            algorithm.fill_finalize  ()
        return algorithm._prev_fill_finalize_()
    # =====================================================================
    ALGO._prev_fill_finalize_ = ALGO.finalize 
    ALGO.finalize             = _algo_fill_finalize_ 

# =============================================================================
if '__main__' == __name__ :
    
    logger.info ( 80*'*') 
    logger.info (  __doc__ ) 
    logger.info ( ' Author  : %s' %        __author__    )
    logger.info ( ' Version : %s' %        __version__   ) 
    logger.info ( ' Date    : %s' %        __date__      ) 
    logger.info ( ' Symbols : %s' % list ( __all__     ) )
    logger.info ( 80*'*')
    methods = ( treatPions     ,
                treatKaons     ,
                treatProtons   ,
                treatMuons     ,
                treatPhotons   ,
                treatDiPhotons ,
                treatTracks    ,
                ##
                treatKine      ,
                fillMasses     ,
                addRecSummary  ,
                ## addGecInfo  ,
                addRecSummary  ,
                annPidTunes    ,
                )
    
    for m in methods :
        logger.info ( '"%s": %s' % (  m.__name__, _doc_ ( m.__doc__ ) ) ) 
    
    logger.info ( 80*'*')

    Tuple = cpp.Tuples.Tuple
    from Ostap.Utils import mute
    with mute ( True , True ) :
        import Bender.Main
        good = lambda i :  ( 0 <= i.upper().find('ARRAY') or 0 <= i.upper().find('COLUMN') or  0 <= i.upper().find('MATRIX') or i.upper().find('ADD') or i.upper().find('INFO') ) and not i.startswith('_') 
        data = [ i for i in dir(Tuple) if  good(i) ]
        import Bender.MainMC
        mc   = [ i for i in dir(Tuple) if  good(i) and not i in data ]
        data.sort()
        mc.sort  () 
        
    logger.info ( 80*'*')
    logger.info ('Collection of important   methods of Tuple: %s' % data )
    for m in data :
        a = getattr ( Tuple , m )
        logger.info ( '"%s": %s' % ( m , _doc_ ( a.__doc__ ) ) ) 
    
    logger.info ('Collection of MC-specific methods of Tuple: %s' % mc   )
    for m in mc :
        a = getattr ( Tuple , m )
        logger.info ( '"%s": %s' % ( m , _doc_ ( a.__doc__ ) ) )
    
    logger.info ( 80*'*')
    logger.info ('Summary:') 
    logger.info ('Tuple-related methods of Algo :\n%s' % [ m.__name__ for m in methods ] )
    logger.info ('Important     methods of Tuple:\n%s' % data )
    logger.info ('MC-specific   methods of Tuple:\n%s' % mc   )
    logger.info ( 80*'*')
                

# ==============================================================================
# The END 
# ==============================================================================
