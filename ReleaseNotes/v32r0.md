
# 2018-03-16 Bender v32r0
=========================

This version is released on '2018-patches' branch of DaVinci and 'master' branch of Bender 

It is based on DaVinci [v44r0](http://lhcbdoc.web.cern.ch/lhcbdoc/davinci/releases/v44r0)

  - new examples for Turbo, Turbo/MC, Turbo/uMC are provided 
  - BenderTools.Fill : add new  variables (CaloHypoSpdM)  for Photons 
  - add `CheckAnnPID` algorithms
  - extend bookkeeping interface 
  - remove extra prints 
  - new Github-based [Bender Tutorials](https://lhcb.github.io/bender-tutorials) 
    * many thanks to Chris Burr for the kind help 